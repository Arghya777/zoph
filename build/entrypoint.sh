echo Creating config for Zoph
echo [zoph]
echo db_host = ${DB_HOST-'sql'}
echo db_name = ${DB_NAME-'zoph'}
echo db_user = ${DB_USER-'zoph'}
echo db_pass = '*****'
echo db_prefix = ${DB_PREFIX-'zoph_'}
echo php_location = ${PHP_LOC-'/var/www/html'}

echo "date.timezone: " $TZ >> /usr/local/etc/php/conf.d/zoph.ini
cat > /etc/zoph.ini << END
[zoph]
db_host = ${DB_HOST-'sql'}
db_name = ${DB_NAME-'zoph'}
db_user	= ${DB_USER-'zoph'}
db_pass = ${DB_PASS}
db_prefix = ${DB_PREFIX-'zoph_'}
php_location = ${PHP_LOC-'/var/www/html'}
END

docker-php-entrypoint
apache2-foreground
