<?php
/**
 * Import controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use import\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the import controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class importControllerTest extends TestCase {

    /**
     * Setup testcase
     * Enable import & upload
     */
    public function setUp() : void {
        parent::setUp();
        conf::set("import.enable", 1);
        conf::set("import.upload", 1);
    }

    /**
     * Disable import & upload
     */
    protected function tearDown() : void {
        conf::set("import.enable", 0);
        conf::set("import.upload", 0);
    }

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test display action
     */
    public function testActionDisplay() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();
        $this->assertInstanceOf(import\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());

        $this->assertStringContainsString("<div class=\"import_uploads\">", (string) $output);
        $this->assertStringContainsString("<div id=\"import_details\" class=\"import_details\">", (string) $output);
        $this->assertStringContainsString("<div id=\"import_thumbs\" class=\"import_thumbs\">", (string) $output);
    }

    /**
     * Test display action - import disabled
     */
    public function testActionDisplayImportDisabled() {
        conf::set("import.enable", 0);
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

    }

    /**
     * Test display action - import not allowed
     */
    public function testActionDisplayImportUserNotAllowed() {
        user::setCurrent(new user(3));
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);
        user::setCurrent(new user(1));
    }

    public function getActions() {
        return array(
            array("display", import\view\display::class),
            array("", import\view\display::class),
            array("nonexistent", import\view\display::class)
        );
    }

}
