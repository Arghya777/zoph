<?php
/**
 * Zoph controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use zoph\controller;
use PHPUnit\Framework\TestCase;
use web\request;


/**
 * Test the zoph controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

class zophControllerTest extends TestCase {
    /**
     * Test action display // shows the first (welcome) page after login
     */
    public function testActionDisplay() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(\zoph\view\display::class, $view);
        // Random photos + link
        $this->assertStringContainsString("Random photos", (string) $view->view());
        $this->assertStringContainsString("photos.php", (string) $view->view());

        // changed photos + recent photos 
        $this->assertStringContainsString("Recently changed photos", (string) $view->view());
        $this->assertStringContainsString("Most recent photos", (string) $view->view());
        
        // recent photos + link
        $this->assertStringContainsString("Newest albums", (string) $view->view());
        $this->assertStringContainsString("album.php", (string) $view->view());
        

        $this->assertEquals("Zoph Test", $view->getTitle());
    }

    /**
     * Test action reports
     */
    public function testActionReports() {
        $request=new request(array(
            "GET"   => array("_action" => "reports"),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(\zoph\view\reports::class, $view);
        $this->assertStringContainsString("Most Populated Albums", (string) $view->view());
        $this->assertStringContainsString("Most Populated Categories", (string) $view->view());
        $this->assertEquals("Reports", $view->getTitle());
    }

    /**
     * Test action login
     */
    public function testActionLogin() {
        $request=new request(array(
            "GET"   => array("_action" => "login"),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(\zoph\view\login::class, $view);

        // This view outputs directly in display().
        $this->assertNull($view->view());
    }

    /**
     * Test action info
     */
    public function testActionInfo() {
        $request=new request(array(
            "GET"   => array("_action" => "info"),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(\zoph\view\info::class, $view);
        $this->assertStringContainsString("Zoph stands for <strong>z</strong>oph <strong>o</strong>rganizes <strong>ph</strong>otos." , (string) $view->view());
        $this->assertStringContainsString("Zoph Test", $view->getTitle());
    }
}

