<?php
/**
 * Photos View Prepare test (download feature)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photos\view\prepare;
use photos\params;
use photo\collection;
use PHPUnit\Framework\TestCase;
use web\request;
use conf\conf;

/**
 * Test the photos view prepare class (download feature)
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosViewPrepareTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $prepare = new prepare($request, $params);
        $this->assertInstanceOf('\photos\view\prepare', $prepare);
        return $prepare;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(prepare $prepare) {
        $tpl=$prepare->view();
        $this->assertInstanceOf('\template\template', $tpl);
    }

    /**
     * Test create view with photos
     */
    public function testCreateWithPhotos() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "album_id" => 5,
                "_action"   => "prepare",
                "_filename" => "test",
                "_maxfiles" => "2",
                "_maxsize"  => "50000"
            ),
            "SERVER" => array()
        ));
        $params = new params($request);

        $collection = collection::createFromRequest($request);


        $prepare = new prepare($request, $params);
        $prepare->setPhotos($collection);
        $prepare->setDisplay($collection);
        $prepare->setFile("test", 1, 2);


        $this->assertInstanceOf('\photos\view\prepare', $prepare);
        return $prepare;
    }

    /**
     * Test view with photos
     *
     * @depends testCreateWithPhotos
     */
    public function testViewWithPhotos(prepare $prepare) {
        $tpl=$prepare->view();
        $this->assertInstanceOf('\template\template', $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('<iframe class="download" title="download" src="photos.php?_action=zipfile&_filename=test&_filenum=1"></iframe>', $act);
        $this->assertStringContainsString('The zipfile is being created... Downloaded 2 of 3 photos', $act);
        $this->assertStringContainsString('photos.php?album_id=5&_action=prepare&_filename=test&_maxfiles=2&_maxsize=50000&_off=2&_filenum=2', $act);
    }

    /**
     * Test create view with photos - download the second part
     */
    public function testCreateNextWithPhotos() {
        $request=new request(array(
            "GET"   => array(
                "album_id" => 5,
                "_action"   => "prepare",
                "_filename" => "test",
                "_maxfiles" => "2",
                "_maxsize"  => "50000",
                "_off"      => "2",
                "_filenum"  => "2"
            ),
            "POST"  => array(
            ),
            "SERVER" => array()
        ));
        $params = new params($request);

        $collection = collection::createFromRequest($request);


        $prepare = new prepare($request, $params);
        $prepare->setPhotos($collection);
        $prepare->setDisplay($collection);
        $prepare->setFile("test", 2, 1);


        $this->assertInstanceOf('\photos\view\prepare', $prepare);
        return $prepare;
    }

    /**
     * Test view with photos
     *
     * @depends testCreateNextWithPhotos
     */
    public function testViewNextWithPhotos(prepare $prepare) {
        $tpl=$prepare->view();
        $this->assertInstanceOf('\template\template', $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('<iframe class="download" title="download" src="photos.php?_action=zipfile&_filename=test&_filenum=2"></iframe>', $act);
        $this->assertStringContainsString('All photos have been downloaded in 2 zipfiles', $act);
        $this->assertStringContainsString('album_id=5">Go back</a>', $act);
    }

}
