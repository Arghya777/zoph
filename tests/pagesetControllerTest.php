<?php
/**
 * Pageset controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use pageset\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the pageset controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class pagesetControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Create pageset
     */
    public function testInsertAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "insert",
                "pageset_id"    => "",
                "title"         => "Set of pages",
                "show_orig"     => "never",
                "orig_pos"      => "top"
            ),
            "SERVER" => array(
                "REMOTE_ADDR"   => "1.2.3.4"
            )
        ));

        $controller = new controller($request);

        $pageset=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: pageset.php?pageset_id=" . $pageset->getId()), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $pageset;
    }

    /**
     * Test the "display" action
     * @depends testInsertAction
     */
    public function testDisplayAction(pageset $pageset) {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "pageset_id"  => $pageset->getId()
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Set of pages", (string) $template);
        $this->assertStringContainsString("user.php?user_id=1", (string) $template);
        $this->assertStringContainsString("pageset.php?_action=edit&amp;pageset_id=" . $pageset->getId(), (string) $template);
    }

    /**
     * Test the "pagesets" action
     * @depends testInsertAction
     */
    public function testPagesetsAction(pageset $pageset) {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "pagesets",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\pagesets::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<table class=\"pagesets\">", (string) $template);
        $this->assertStringContainsString("Set of pages", (string) $template);
        $this->assertStringContainsString("pageset.php?pageset_id=" . $pageset->getId(), (string) $template);
    }

    /**
     * Test the "delete" action
     * @depends testInsertAction
     */
    public function testDeleteAction(pageset $pageset) {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "pageset_id"  => $pageset->getId()
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm deletion of 'Set of pages'", (string) $template);
        $this->assertStringContainsString("pageset.php?_action=confirm&amp;pageset_id=" . $pageset->getId(), (string) $template);
    }

    /**
     * Test the "edit", "delete" and "confirm" action - by non-admin user
     * @depends testInsertAction
     * @dataProvider getEditDeleteConfirmAction
     */
    public function testEditDeleteConfirmUnauthorisedAction(string $action, pageset $pageset) {
        user::setCurrent(new user(5));

        $request=new request(array(
            "GET"   => array(
                "_action"   => $action,
                "pageset_id"  => $pageset->getId()
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\display::class, $view);

        $pagesetIds=array_map(function($p) { return $p->getId(); }, pageset::getAll());
        $this->assertContains($pageset->getId(), $pagesetIds);

        user::setCurrent(new user(1));
    }

    /**
     * Update pageset - add page
     * @depends testInsertAction
     */
    public function testAddPageAction(pageset $pageset) {
        $pageIds = array();

        $page = new page();
        $page->set("title", "Test Page");
        $page->set("text", "[h1]Test Page[/h1]");
        $page->insert();

        $pageIds[] = $page->getId();

        $pageset->addPage($page);

        $page = new page();
        $page->set("title", "Another Page");
        $page->set("text", "[h2]Also a page[/h2]");
        $page->insert();

        $pageIds[] = $page->getId();

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "addpage",
                "pageset_id"    => $pageset->getId(),
                "page_id"    => $page->getId(),
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $pageset=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Test Page", (string) $template);
        $this->assertStringContainsString("Another Page", (string) $template);
        $this->assertEquals(2, $pageset->getPageCount());

        $this->assertEquals($pageIds[0], ($pageset->getPages(0))[0]->getId());
        $this->assertEquals($pageIds[1], ($pageset->getPages(1))[0]->getId());

        return $pageset;
    }

    /**
     * Update pageset - move page up/down
     * @depends testAddPageAction
     * @dataProvider getUpDownAction
     */
    public function testMoveUpDownAction(string $action, pageset $pageset) {
        $pageIds = array(
            ($pageset->getPages(0))[0]->getId(),
            ($pageset->getPages(1))[0]->getId()
        );

        $expPageIds = array(
            ($pageset->getPages(1))[0]->getId(),
            ($pageset->getPages(0))[0]->getId()
        );

        if ($action == "moveup") {
            $page_id = $pageIds[1];
        } else {
            $page_id = $pageIds[0];
        }

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => $action,
                "pageset_id"    => $pageset->getId(),
                "page_id"       => $page_id
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $pageset=$controller->getObject();
        $this->assertInstanceOf(pageset::class, $pageset);

        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\display::class, $view);

        $pageIds = array(
            ($pageset->getPages(0))[0]->getId(),
            ($pageset->getPages(1))[0]->getId()
        );

        $this->assertEquals($pageIds[0], ($pageset->getPages(0))[0]->getId());
        $this->assertEquals($pageIds[1], ($pageset->getPages(1))[0]->getId());
    }

    /**
     * Update pageset - remove pages
     * @depends testAddPageAction
     */
    public function testRemoveAction(pageset $pageset) {
        $pageIds = array(
            ($pageset->getPages(0))[0]->getId(),
            ($pageset->getPages(1))[0]->getId()
        );

        foreach ($pageIds as $page_id) {
            $request=new request(array(
                "GET"   => array(),
                "POST"  => array(
                    "_action"       => "remove",
                    "pageset_id"    => $pageset->getId(),
                    "page_id"       => $page_id
                ),
                "SERVER" => array()
            ));

            $controller = new controller($request);

            $pageset=$controller->getObject();

            $view=$controller->getView();
            $this->assertInstanceOf(pageset\view\display::class, $view);

        }

        $this->assertEmpty($pageset->getPages());

        // Cleanup
        foreach ($pageIds as $page_id) {
            $page = new page($page_id);
            $page->delete();
        }

        return $pageset;
    }

    /**
     * Update pageset
     * @depends testInsertAction
     */
    public function testUpdateAction(pageset $pageset) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "pageset_id"    => $pageset->getId(),
                "title"       => "Set of multiple pages",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $pageset=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\display::class, $view);

        $this->assertEquals("Set of multiple pages", $pageset->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Set of multiple pages", (string) $template);
        $this->assertStringNotContainsString("Set of pages", (string) $template);
        $this->assertStringContainsString("user.php?user_id=1", (string) $template);
        $this->assertStringContainsString("pageset.php?_action=edit&amp;pageset_id=" . $pageset->getId(), (string) $template);

        return $pageset;
    }

    /**
     * Test update by unauthorised user
     * @depends testUpdateAction
     */
    public function testUpdateNotAuthorised(pageset $pageset) {
        user::setCurrent(new user(5));

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "pageset_id"    => $pageset->getId(),
                "title"       => "hacked",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $pageset=$controller->getObject();

        $this->assertInstanceOf(pageset\view\display::class, $view);

        $this->assertEquals("Set of multiple pages", $pageset->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Set of multiple pages", (string) $template);
        $this->assertStringNotContainsString("hacked", (string) $template);

        user::setCurrent(new user(1));
        return $pageset;
    }

    /**
     * Delete pageset
     * @depends testUpdateNotAuthorised
     */
    public function testConfirmAction(pageset $pageset) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "confirm",
                "pageset_id"    => $pageset->getId(),
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: pageset.php?_action=pagesets"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        $pagesetIds=array_map(function($p) { return $p->getId(); }, pageset::getAll());
        $this->assertNotContains($pageset->getId(), $pagesetIds);
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "new"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $pageset=$controller->getObject();
        $this->assertInstanceOf(pageset::class, $pageset);
        $this->assertEquals(0, $pageset->getId());

        $view=$controller->getView();
        $this->assertInstanceOf(pageset\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"pageset_id\" value=\"\">", (string) $template);
    }

    public function getActions() {
        return array(
            array("new", pageset\view\update::class),
            array("edit",pageset\view\update::class),
            array("delete", pageset\view\confirm::class),
            array("display", pageset\view\display::class),
            array("nonexistant", pageset\view\display::class)
        );
    }

    public function getUpDownAction() {
        return [["moveup"], ["movedown"]];
    }

    public function getEditDeleteConfirmAction() {
        return [["edit"], ["delete"], ["confirm"]];
    }
}
