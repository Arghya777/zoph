<?php
/**
 * Category controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use category\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the category controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class categoryControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    protected function tearDown() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "display", "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "category_id"  => 2
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $this->assertInstanceOf(category\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("red", (string) $output);
        $this->assertStringContainsString("category.php?_action=edit&amp;category_id=2", (string) $output);
        $this->assertStringContainsString("<a href=\"photos.php?category_id=2,4,3\">" .
            "<img alt=\"folder\" src=\"templates/default/images/icons/folderphoto.png\">" .
            "<span class=\"photocount\">5 photos</span></a>", (string) $output);
        $this->assertStringContainsString("category.php?category_id=4", (string) $output);
    }

    /**
     * Test the "display" action - unauthorised
     */
    public function testUnauthorisedDisplayAction() {
        user::setCurrent(new user(6));
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "category_id"  => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(category\view\notfound::class, $view);

        $this->assertEquals("Category not found", $view->getTitle());

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("Category not found", (string) $output);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "category_id"  => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(category\view\confirm::class, $view);

        $this->assertEmpty($view->getHeaders());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete category", (string) $template);
        $this->assertStringContainsString("category.php?_action=confirm&amp;category_id=1", (string) $template);
    }

    /**
     * Create category in the db
     */
    public function testInsertAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"           => "insert",
                "parent_category_id"   => "12",
                "category"             => "Concert Pics"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $category=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(category\view\update::class, $view);

        $this->assertEquals("Concert Pics", $category->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("category.php?category_id=" . $category->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"category_id\" value=\"" . $category->getId() . "\">", (string) $template);
        $this->assertStringContainsString("Concert Pics", (string) $template);

        return $category;
    }

    /**
     * Update category in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(category $category) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "category_id"      => $category->getId(),
                "category"         => "Tour Photos",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $category=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(category\view\update::class, $view);

        $this->assertEquals("Tour Photos", $category->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("category.php?category_id=" . $category->getId(), (string) $template);
        $this->assertStringContainsString("Tour Photos", (string) $template);
        $this->assertStringNotContainsString("Concert Pics", (string) $template);

        return $category;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "new",
                "parent_category_id"   => "12",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $category=$controller->getObject();
        $this->assertInstanceOf(category::class, $category);
        $this->assertEquals(0, $category->getId());

        $view=$controller->getView();
        $this->assertInstanceOf(category\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("category.php?category_id=12", (string) $template);
        $this->assertStringContainsString("new category", (string) $template);
        $this->assertStringContainsString("new category", $view->getTitle());
    }

    /**
     * Test confirm (delete) action
     * @depends testUpdateAction
     */
    public function testConfirmAction(category $category) {
        $id=$category->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "confirm",
                "category_id"      => $id,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $categories=category::getAll();
        $ids=array();
        foreach ($categories as $category) {
            $ids[]=$category->getId();
        }
        $this->assertNotContains($id, $ids);

        $category=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: category.php?category_id=12"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $category;
    }

    /**
     * Test set coverphoto
     */
    public function testCoverPhotoAction() {
        $category = new category(12);
        $category->lookup();
        $this->assertEquals($category->get("coverphoto"), null);

        unset($category);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"    => "coverphoto",
                "category_id"   => "12",
                "coverphoto" => "1",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $category=$controller->getObject();
        $this->assertInstanceOf(category::class, $category);
        $this->assertEquals($category->get("coverphoto"), 1);

        $view=$controller->getView();

        $this->assertInstanceOf(category\view\display::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<img src=\"image.php?photo_id=1&amp;type=thumb\" class=\"thumb\"", (string) $template);

        return $category;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(category $category) {
        $id=$category->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "unsetcoverphoto",
                "category_id"      => $id,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $category=$controller->getObject();
        $this->assertInstanceOf(category::class, $category);
        $this->assertEquals($category->get("coverphoto"), null);

        $this->assertInstanceOf(category\view\display::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringNotContainsString("<img src=\"image.php?photo_id=1&amp;type=thumb\" class=\"thumb\"", (string) $template);
    }

    public function getActions() {
        return array(
            array("display", category\view\display::class),
            array("new", category\view\update::class),
            array("edit", category\view\update::class),
            array("delete", category\view\confirm::class),
            array("nonexistant", category\view\display::class)
        );
    }
}
