<?php
/**
 * Group test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";
use PHPUnit\Framework\TestCase;

/**
 * Test the group class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class groupTest extends TestCase {

    /**
     * Create groups in the db
     * @dataProvider getGroupData();
     */
    public function testCreateGroups($name, array $members) {
        $group=new group();
        $group->set("group_name", $name);
        $group->insert();
        foreach ($members as $member) {
            $user=user::getByName($member);
            $group->addMember($user);
        }

        $this->assertInstanceOf("group", $group);
        $this->assertEquals(sizeof($members), sizeof($group->getMembers()));
        $this->assertEquals($name, $group->getName());

        $group->delete();
    }

    /**
     * Test getAlbums() function
     * @dataProvider getGroupAlbums();
     */
    public function testGetAlbums($groupId, array $expAlbumids) {
        $group=new group($groupId);
        $albums=$group->getAlbums();

        $actAlbumids=array();
        foreach ($albums as $album) {
            $actAlbumids[]=$album->getId();
        }

        $this->assertEquals($expAlbumids, $actAlbumids);
    }

    /**
     * Test get_members() function
     * @dataProvider getGroupMembers();
     */
    public function testGetMembers($groupId, array $expUserIds) {
        $group=new group($groupId);
        $users=$group->getMembers();

        $actUserIds=array();
        foreach ($users as $user) {
            $actUserIds[]=$user->getId();
        }

        $this->assertEquals($expUserIds, $actUserIds);
    }

    /**
     * Test remove_members() function
     */
    public function testRemoveMembers() {
        $group=new group(1);
        foreach ([2,5,7] as $memberId) {
            $group->removeMember(new user($memberId));
        }

        $users=$group->getMembers();
        foreach ($users as $user) {
            $actUserIds[]=$user->getId();
        }

        $this->assertEquals(array(9), $actUserIds);

        foreach ([2,5,7] as $memberId) {
            $group->addMember(new user($memberId));
        }
    }

    /**
     * Test group::getByName() function
     */
    public function testGetByName() {
        $group = group::getByName("Guitarists");
        $group->lookup();
        $this->assertInstanceOf("group", $group);
        $this->assertEquals("guitarists", $group->getName());
    }

    /**
     * Test group::exists() function
     */
    public function testExists() {
        $exists=group::exists("Guitarists");
        $this->assertTrue($exists);
        $exists=group::exists("Xylophonists");
        $this->assertFalse($exists);
    }

    public function getGroupData() {
        return array(
            array("TestGroup", array("freddie", "johnd","brian","roger")),
            array("AnotherTestGroup", array("paul","jimi"))
        );
    }

    public function getGroupAlbums() {
        return array(
            array(1, array(1,2)),
            array(2, array(1,2,3)),
            array(3, array())
        );
    }

    public function getGroupMembers() {
        return array(
            array(1, array(2,5,7,9)),
            array(2, array(4,8)),
            array(4, array(2,3))
        );
    }

}
