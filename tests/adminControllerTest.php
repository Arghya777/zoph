<?php
/**
 * Admin controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use admin\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the admin controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class adminControllerTest extends TestCase {

    /**
     * Test the "display" action
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(admin\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);
        $output = helpers::whitespaceClean((string) $template);
        
        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<h1>Adminpage</h1>", $output);
        $this->assertStringContainsString("users", $output);
        $this->assertStringContainsString("groups", $output);
        $this->assertStringContainsString("config.php", $output);
    }

    public function getActions() {
        return array(
            array("display", admin\view\display::class),
            array("nonexistant", admin\view\display::class)
        );
    }

}
