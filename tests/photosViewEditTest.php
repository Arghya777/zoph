<?php
/**
 * Phots View Edit test (bulk photo edit feature)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photos\view\edit;
use photos\params;
use photo\collection;
use PHPUnit\Framework\TestCase;
use web\request;
use conf\conf;

/**
 * Test the photos view edit class (bulk edit feature)
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photosViewEditTest extends TestCase {

    /**
     * Test creating a View
     */
    public function testCreate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $edit = new edit($request, $params);
        $this->assertInstanceOf('\photos\view\edit', $edit);
        return $edit;
    }

    /**
     * Test view
     *
     * @depends testCreate
     */
    public function testView(edit $edit) {
        $tpl=$edit->view();
        $this->assertInstanceOf('\template\template', $tpl);
    }

    /**
     * Test create view with photos
     */
    public function testCreateWithPhotos() {
        $request=new request(array(
            "GET"   => array("album_id" => 2),
            "POST"  => array(),
            "SERVER" => array()
        ));
        $params = new params($request);

        $collection = collection::createFromRequest($request);


        $edit = new edit($request, $params);
        $edit->setPhotos($collection);
        $edit->setDisplay($collection);


        $this->assertInstanceOf('\photos\view\edit', $edit);
        return $edit;
    }

    /**
     * Test view with photos
     *
     * @depends testCreateWithPhotos
     */
    public function testViewWithPhotos(edit $edit) {
        $tpl=$edit->view();
        $this->assertInstanceOf('\template\template', $tpl);

        // Remove enters and duplicate whitespace, to ease comparison
        $act = helpers::whitespaceClean((string) $tpl);

        // Checking for a few strings in the template, to make sure it built correctly:
        $this->assertStringContainsString('<input type="hidden" name="album_id" value="2">', $act);
        $this->assertStringContainsString('<legend>TEST_0001.JPG</legend>', $act);
        $this->assertStringContainsString('<legend>TEST_0007.JPG</legend>', $act);
        $this->assertStringContainsString('<input type="hidden" id="location_id__1" name="__location_id__1" value="3">', $act);
    }

}
