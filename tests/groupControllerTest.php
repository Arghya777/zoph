<?php
/**
 * Group controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use group\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the group controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class groupControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "group_id"  => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(group\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("group.php?_action=edit&amp;group_id=1", (string) $template);
        $this->assertStringContainsString("Queen", (string) $template);
        $this->assertStringContainsString("freddie", (string) $template);

    }

    /**
     * Test the "groups" action
     */
    public function testGroupsAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "groups"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(group\view\groups::class, $view);

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("group.php?_action=new", (string) $template);
        $this->assertStringContainsString("<dl class=\"groups\">", (string) $template);
        $this->assertStringContainsString("<dt>Queen</dt>", (string) $template);
        $this->assertStringContainsString("<a href='user.php?user_id=2'>brian</a>", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "group_id"  => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(group\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete group", (string) $template);
        $this->assertStringContainsString("group.php?_action=confirm&amp;group_id=1", (string) $template);
    }

    /**
     * Create group in the db
     */
    public function testInsertAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "insert",
                "group_name"    => "The Animals",
                "description"   => "60s rock band"),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $group=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(group\view\update::class, $view);
        $this->assertEquals("The Animals", $group->getName());

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("group.php?group_id=" . $group->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"group_id\" value=\"" . $group->getId() . "\">", (string) $template);
        $this->assertStringContainsString("The Animals", (string) $template);
        $this->assertStringContainsString("60s rock band", (string) $template);

        return $group;
    }

    /**
     * Update group in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(group $group) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "group_id"      => $group->getId(),
                "group_name"    => "Eric Burtons Animals",
                "_member"    =>  2
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $group=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(group\view\update::class, $view);

        $this->assertEquals("Eric Burtons Animals", $group->getName());
        $this->assertEquals(2, $group->getMembers()[0]->getId());

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("group.php?group_id=" . $group->getId(), (string) $template);
        $this->assertStringContainsString("Eric Burtons Animals", (string) $template);
        $this->assertStringNotContainsString("The Animals", (string) $template);
        $this->assertStringContainsString("60s rock band", (string) $template);

        return $group;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "new",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $group=$controller->getObject();
        $this->assertInstanceOf("group", $group);
        $this->assertEquals(0, $group->getId());

        $view=$controller->getView();
        $this->assertInstanceOf(group\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("group.php?_action=groups", (string) $template);
        $this->assertStringContainsString("After this group is created it can be given access to albums.", (string) $template);
        $this->assertStringContainsString("New group", (string) $template);
        $this->assertStringContainsString("New group", $view->getTitle());
    }

    /**
     * Update group, remove member
     * @depends testUpdateAction
     */
    public function testUpdateRemoveMemberAction(group $group) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "group_id"      => $group->getId(),
                "_remove" =>  array(2)
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $group=$controller->getObject();
        $view=$controller->getView();

        $this->assertInstanceOf(group\view\update::class, $view);
        $this->assertEmpty($view->getHeaders());
        $this->assertEquals("Eric Burtons Animals", $group->getName());
        $this->assertEquals(0, sizeof($group->getMembers()));

        return $group;
    }

    /**
     * Test confirm (delete) acrion
     * @depends testUpdateRemoveMemberAction
     */
    public function testConfirmAction(group $group) {
        $id=$group->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "confirm",
                "group_id"      => $id,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $groups=group::getAll();
        $ids=array();
        foreach ($groups as $group) {
            $ids[]=$group->getId();
        }
        $this->assertNotContains($id, $ids);

        $group=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: group.php?_action=groups"), $view->getHeaders());
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $group;
    }

    public function getActions() {
        return array(
            array("new", group\view\update::class),
            array("edit", group\view\update::class),
            array("delete", group\view\confirm::class),
            array("groups", group\view\groups::class),
            array("nonexistant", group\view\display::class)
        );
    }
}
