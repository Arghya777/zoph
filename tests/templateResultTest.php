<?php
/**
 * Test result - a template to show results
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use template\result;
use PHPUnit\Framework\TestCase;

/**
 * Test result template
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class templateResultTest extends TestCase {
    /**
     * Test result
     * @dataProvider getResults
     */
    public function testResult(int $status, string $msg, string $error, string $exp_status, string $icon = null) {

        $result = new result($status, $msg, $error);

        $this->assertInstanceOf('template\result', $result);

        $output = (string) $result;

        $this->assertStringContainsString("icons/" . $icon ?: $exp_status . ".png", $output);
        $this->assertStringContainsString("class=\"result " . $exp_status . "\"", $output);
        $this->assertStringContainsString($msg, $output);
        if (!empty($error)) {
            $this->assertStringContainsString($error, $output);

        }
    }

    public function getResults() {
        return array(
            array(result::TODO, "Upgrade to do", "", "todo", "todo"),
            array(result::SUCCESS, "It all went well", "", "ok"),
            array(result::INFO, "I have something to say", "", "info"),
            array(result::WARNING, "There were some issues", "Warning!", "warning"),
            array(result::ERROR, "It all went haywire", "Run!", "error"),
            array(99, "I don't know what happened", "", "unknown")
        );
    }

}
