<?php
/**
 * Install controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use auth\validator;
use db\db;
use conf\conf;
use install\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the install controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class installControllerTest extends TestCase {

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    public function testDisplay() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<section class=\"install\">", (string) $output);

        $this->assertStringContainsString("<a href=\"install.php?_action=requirements\" class=\"button install\" target=\"\">", (string) $output);
    }

    public function testRequirements() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "requirements",
                ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\requirements::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        $this->assertStringContainsString("<table class=\"requirements\">", (string) $output);
        $this->assertStringContainsString("class=\"reqpass\"", (string) $output);
        $this->assertStringContainsString("Check for PHP FileInfo Extension", (string) $output);
        $this->assertStringContainsString("PHP version supported", (string) $output);
    }

    public function testIniform() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "iniform",
                ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\iniform::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"name\"", (string) $output);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"_action\" value=\"inifile\">",
            (string) $output);
    }

    public function testInifile() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "inifile",
                "name"      => "zoph-install",
                "db_host"   => "db.zoph.org",
                "db_name"   => "install",
                "db_user"   => "test",
                "db_pass"   => "secret",
                "db_prefix" => "zoph_"
                ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\inifile::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        // Check if the generated INI file is consisent with the input:
        $this->assertStringContainsString("&#091;zoph-install&#093", (string) $output);
        $this->assertStringContainsString("db_host = &quot;db.zoph.org&quot;", (string) $output);
        $this->assertStringContainsString("db_name = &quot;install&quot;", (string) $output);
        $this->assertStringContainsString("db_user = &quot;test&quot;", (string) $output);
        $this->assertStringContainsString("db_pass = &quot;secret&quot;", (string) $output);
        $this->assertStringContainsString("db_prefix = &quot;zoph_&quot;", (string) $output);
    }

    public function testDbPass() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "dbpass",
                ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\dbpass::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"admin_pass\" type=\"password\" name=\"admin_pass\" size=\"32\">", (string) $output);
    }

    public function testDbCheck() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "dbcheck",
                ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\dbcheck::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        // The check fails during unittest run, because the configuration for is not
        // stored in the INI file, like during normal run.
        $this->assertStringContainsString("Problem with INI file", (string) $output);
    }

    public function testDatabase() {

        $db = db::getLoginDetails();

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "database",
                "user"          => "admin",
                "admin_pass"   => "VerySecret"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\database::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<div class=\"result ok\">", (string) $output);
        $this->assertStringContainsString("create", (string) $output);
        $this->assertStringContainsString("grant", (string) $output);

        db::setLoginDetails($db["host"], $db["dbname"], $db["user"], $db["pass"], $db["prefix"]);
    }

    public function testTables() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "tables",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\tables::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<div class=\"result ok\">", (string) $output);
        $this->assertStringContainsString("photos", (string) $output);
        $this->assertStringContainsString("albums", (string) $output);
        $this->assertStringContainsString("places", (string) $output);
    }

    public function testAdminuser() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "adminuser",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(install\view\adminuser::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"admin_pass\" type=\"password\" name=\"admin_pass\" size=\"32\">", (string) $output);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"_action\" value=\"config\">", (string) $output);
    }

    public function testConfig() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "config",
                "admin_pass"    => "S3cr3t",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        // Restore configuration
        conf::set("path.images", getcwd() . "/.images")->update();

        $view = $controller->getView();
        $this->assertInstanceOf(install\view\config::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input type=\"text\" pattern=\"^.*$\" name=\"interface.title\" value=\"Zoph\" size=\"30\">", (string) $output);
        $this->assertStringContainsString("<select name=\"maps.provider\">", (string) $output);

        // Check if the password has changed
        $user = (new validator("admin", "S3cr3t"))->validate();
        $this->assertInstanceOf('user', $user);

        // Restore password
        $user->set("password", validator::hashPassword("admin"));
        $user->update();

    }

    public function testFinish() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"           => "finish",
                "interface.title"   => "Zoph Test",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        // Restore configuration
        conf::set("path.images", getcwd() . "/.images")->update();

        $view = $controller->getView();
        $this->assertInstanceOf(install\view\finish::class, $view);

        $title = conf::get("interface.title");
        $this->assertEquals("Zoph Test", $title);
    }

    public function getActions() {
        return array(
            array("display", install\view\display::class),
            array("requirements", install\view\requirements::class),
            array("iniform", install\view\iniform::class),
            array("inifile", install\view\inifile::class),
            array("dbpass", install\view\dbpass::class),
            array("nonexistent", install\view\display::class)
        );
    }
}
