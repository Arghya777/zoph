<?php
/**
 * Web Service controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use web\service\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the web service controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class webServiceControllerTest extends TestCase {

    /**
     * Test location lookup action
     */
    public function testActionLocationLookup() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "locationLookup",
                "search"    => "7GXHX4HM MM"
            ),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(29.9791875, $json["lat"]);
        $this->assertEquals(31.1341875, $json["lon"]);
        $this->assertEquals(17, $json["zoom"]);
    }

    /**
     * Test photoData action
     */
    public function testActionPhotoData() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoData",
                "photoId"  => 3
            ),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals("TEST_0003.JPG", $json["name"]);
        $this->assertEquals("Rotterdam", $json["location"]);
        $this->assertCount(2, $json["albums"]);
        $this->assertCount(2, $json["categories"]);
        $this->assertCount(2, $json["people"]);
    }

    /**
     * Test search action
     */
    public function testActionSearch() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "search",
                "album_id"  => 3
            ),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals([1,2,8], $json);
    }

    /**
     * Test photopeople with no action
     * (returns the current people on the photo)
     */
    public function testActionPhotoPeopleActionEmpty() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "photoId"  => "2",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "left" action
     * (moves a person to the left)
     */
    public function testActionPhotoPeopleActionLeft() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "left",
                "photoId"  => "2",
                "personId"  => "7"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][0]["name"]); // swapped
        $this->assertEquals(5, $json["people"][1][1]["id"]);                // swapped
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "right" action
     * (moves a person to the right)
     */
    public function testActionPhotoPeopleActionRight() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "right",
                "photoId"  => "2",
                "personId"  => "7"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);                // swapped back
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]); // swapped back
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "up" action
     * (moves a person a row up)
     */
    public function testActionPhotoPeopleActionUp() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "up",
                "photoId"   => "2",
                "personId"  => "9"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(2, $json["people"][0][1]["position"]);         // moved up
        $this->assertEquals("John Deacon", $json["people"][0][1]["name"]); // moved up
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
    }

    /**
     * Test photopeople with "down" action
     * (moves a person a row down)
     */
    public function testActionPhotoPeopleActionDown() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "down",
                "photoId"   => "2",
                "personId"  => "9"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
        $this->assertEquals(3, $json["people"][1][2]["position"]);         // moved back down
        $this->assertEquals("John Deacon", $json["people"][1][2]["name"]); // moved back down
    }

    /**
     * Test photopeople with "add" action
     * (adds a person)
     */
    public function testActionPhotoPeopleActionAdd() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "add",
                "photoId"  => "2",
                "personId"  => "3",
                "row"       => "3"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(3, $json["people"][2][0]["id"]);
    }

    /**
     * Test photopeople with "remove" action
     * (removes a person)
     */
    public function testActionPhotoPeopleActionRemove() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "photoPeople",
                "action"    => "remove",
                "photoId"  => "2",
                "personId"  => "3"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertArrayNotHasKey(2, $json["people"]);
    }

    /**
     * Test translate action
     */
    public function testActionTranslate() {
        global $lang;
        $curLang = clone $lang;
        $lang = new language("nl");
        $lang->read();
        $request=new request(array(
            "GET"   => array(
                "_action"   => "translation",
            ),
            "POST"  => array(),
            "SERVER" => array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals("Foto", $json["Photo"]);
        $lang = $curLang;
    }
}
