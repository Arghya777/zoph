<?php
/**
 * Photo controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photo\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the photo controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoControllerTest extends TestCase {

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test display action
     */
    public function testActionDisplay() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "photo_id"  => "1"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $this->assertInstanceOf(photo\view\display::class, $controller->getView());
    }

    /**
     * Test display action
     */
    public function testActionDisplayOffset() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "album_id"  => "2",
                "_off"      => "1"
            ),
            "POST"  => array(),
            "SERVER" => array(
                "PHP_SELF"          => "photo.php",
                "QUERY_STRING"  => "_action=display&album_id=2&_off=1"
            )
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(photo\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("image.php?photo_id=7", (string) $output);
        $this->assertStringContainsString("photo.php?album_id=2&amp;_off=0", (string) $output); // Prev link
        $this->assertStringContainsString("photos.php?album_id=2&amp;_off=0", (string) $output); // Up link
        $this->assertStringNotContainsString("photo.php?album_id=2&amp;_off=2", (string) $output); // No Next link
    }

    /**
     * Test update action
     */
    public function testActionUpdate() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "update",
                "photo_id"  => "1",
                "title"     => "Updated by testActionUpdate"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(photo\view\display::class, $view);

        $photo=new photo(1);
        $photo->lookup();

        $this->assertEquals("Updated by testActionUpdate", $photo->get("title"));
    }

    /**
     * Test delete action
     * delete does not delete the photo, it shows the 'confirm' view.
     */
    public function testActionDelete() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "photo_id"  => "1"
            ),
            "POST"  => array(),
            "SERVER" => array(
                "PHP_SELF"          => "photo.php",
                "QUERY_STRING"      => "_action=delete&photo_id=1"
            )
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(photo\view\confirm::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("photo.php?_action=confirm&amp;photo_id=1", (string) $output);
        $this->assertStringContainsString("photo.php?photo_id=1", (string) $output); // Prev link

    }

    /**
     * Test confirm action
     */
    public function testActionConfirm() {
        $photo = new photo();
        $photo->insert();
        $photo_id = $photo->getId();

        $request=new request(array(
            "GET"   => array(
                "_action"   => "confirm",
                "photo_id"  => $photo_id
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $photoIds=array_map(function($p) { return $p->getId(); }, photo::getAll());

        $this->assertNotContains($photo_id, $photoIds);

    }

    /**
     * Test rate action
     */
    public function testActionRate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "rate",
                "photo_id"  => "7",
                "rating"     => "10"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: zoph.php"), $view->getHeaders());

        $photo=new photo(7);
        $photo->lookup();

        $this->assertEquals(10, $photo->getRating());

        $ratings=rating::getRatings($photo, user::getCurrent());
        return array_shift($ratings);
    }

    /**
     * Test delrate action
     * @depends testActionRate
     */
    public function testActionDelrate(rating $rating) {
        $rating->lookup();

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "delrate",
                "_rating_id" => $rating->getId(),
                "_return"   => "zoph.php"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view = $controller->getView();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: zoph.php"), $view->getHeaders());

        $photo=new photo(7);
        $photo->lookup();

        $this->assertEquals(null, $photo->getRating());
    }

    /**
     * Test select action
     */
    public function testActionSelect() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "select",
                "photo_id"  => "7",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(photo\view\display::class, $view);

        $this->assertEquals(7, $_SESSION["selected_photo"][0]);
    }

    /**
     * Test deselect action
     */
    public function testActionDeselect() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "deselect",
                "photo_id"  => "7",
                "_return"   => "photo.php",
                "_qs"       => "photo_id=1"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: photo.php?photo_id=1"), $view->getHeaders());

        $this->assertEmpty($_SESSION["selected_photo"]);
    }

    /**
     * Test lightbox action
     */
    public function testActionLightbox() {
        $user = user::getCurrent();
        $user->set("lightbox_id", 2);
        $user->update();

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "lightbox",
                "photo_id"  => "8",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(photo\view\display::class, $view);

        $photo = $controller->getObject();
        $this->assertInstanceOf(photo::class, $photo);
        $this->assertEquals(8, $photo->getId());

        $albumIds=array_map(function($a) { return $a->getId(); }, $photo->getAlbums());
        $this->assertContains(2, $albumIds);
    }

    /**
     * Test unlightbox action
     * @depends testActionLightbox
     */
    public function testActionUnlightbox() {
        $user = user::getCurrent();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"   => "unlightbox",
                "photo_id"  => "8",
                "_qs"       => "album_id=8"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $photo = $controller->getObject();
        $view = $controller->getView();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: photos.php?album_id=8"), $view->getHeaders());

        $albumIds=array_map(function($a) { return $a->getId(); }, $photo->getAlbums());
        $this->assertNotContains(2, $albumIds);

        $user->set("lightbox_id", null);
        $user->update();
    }

    public function getActions() {
        return array(
            array("display", photo\view\display::class),
            array("insert", photo\view\display::class),
            array("new", photo\view\display::class),
            array("edit", photo\view\update::class),
            array("nonexistent", photo\view\display::class)
        );
    }

}
