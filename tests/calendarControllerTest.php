<?php
/**
 * Calendar controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use calendar\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the calendar controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class calendarControllerTest extends TestCase {

    /**
     * Test the "display" action
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $request=new request(array(
            "GET"   => array(
                "date"          => "2014-01-01",
                "search_field"  => "date"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(calendar\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("January 2014", (string) $template);
        $this->assertStringContainsString("calendar.php?month=12&amp;year=2013&amp;search_field=date", (string) $template); // prev link
        $this->assertStringContainsString("calendar.php?month=02&amp;year=2014&amp;search_field=date", (string) $template); // next link
        $this->assertStringContainsString("photos.php?date=2014-01-01", (string) $template); // link for 01 Jan
    }

    /**
     * Test the "display" action with month/year and timestamp
     */
    public function testDisplayActionMonthYearTimestamp() {
        $request=new request(array(
            "GET"   => array(
                "month"          => "01",
                "year"          => "2014",
                "search_field"  => "timestamp"
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(calendar\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf("\\template\\template", $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("January 2014", (string) $template);
        $this->assertStringContainsString("calendar.php?month=12&amp;year=2013&amp;search_field=timestamp", (string) $template); // prev link
        $this->assertStringContainsString("calendar.php?month=02&amp;year=2014&amp;search_field=timestamp", (string) $template); // next link
        $this->assertStringContainsString("photos.php?timestamp%5B0%5D=20140101000000" .
            "&_timestamp_op%5B0%5D=%3E%3D&timestamp%5B1%5D=20140102000000&_timestamp_op%5B1%5D=%3C", (string) $template); // link for 01 Jan
    }

    public function getActions() {
        return array(
            array("display", calendar\view\display::class),
            array("nonexistant", calendar\view\display::class)
        );
    }
}
