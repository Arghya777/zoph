<?php
/**
 * User controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use auth\validator;
use user\controller;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the user controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class userControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "user_id"  => 5
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(user\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user.php?_action=edit&amp;user_id=5", (string) $template);
        $this->assertStringContainsString("freddie", (string) $template);
        $this->assertStringContainsString("Freddie Mercury", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "user_id"  => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(user\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete user", (string) $template);
        $this->assertStringContainsString("user.php?_action=confirm&amp;user_id=1", (string) $template);
    }

    /**
     * Create user in the db
     */
    public function testInsertAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "insert",
                "user_name"    => "eric"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $user=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\display::class, $view);
        $this->assertEquals("eric", $user->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user.php?_action=password&amp;user_id=" . $user->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"" . $user->getId() . "\">", (string) $template);
        $this->assertStringContainsString("eric", (string) $template);

        return $user;
    }

    /**
     * Update user in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(user $user) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "user_id"      => $user->getId(),
                "user_name"    => "ericburton",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $user=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\update::class, $view);

        $this->assertEquals("ericburton", $user->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user.php?_action=password&amp;user_id=" . $user->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"" . $user->getId() . "\">", (string) $template);
        $this->assertStringContainsString("ericburton", (string) $template);

        return $user;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "new",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $user=$controller->getObject();
        $this->assertInstanceOf("user", $user);
        $this->assertEquals(0, $user->getId());

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user.php?_action=users", (string) $template);
        $this->assertStringContainsString("New user", (string) $template);
        $this->assertStringContainsString("New user", $view->getTitle());
    }

    /**
     * Test show users
     */
    public function testUsersAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "users",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\users::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<table class=\"users\">", (string) $template);
        $this->assertStringContainsString("Users", $view->getTitle());
        $this->assertStringContainsString("freddie", (string) $template);
        $this->assertStringContainsString("Freddie Mercury", (string) $template);
        $this->assertStringContainsString("user.php?user_id=5", (string) $template);
        $this->assertStringContainsString("person.php?person_id=5", (string) $template);
    }

    /**
     * Test update by non-admin user
     */
    public function testUpdateNotAuthorized() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "user_id"      => 3,
                "user_name"    => "hacked",
                "_password"      => "hacked"
            ),
            "SERVER" => array()
        ));

        $testUser = new user(3);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $controller = new controller($request);


        $view=$controller->getView();
        $this->assertInstanceOf(user\view\password::class, $view);

        // Verify username and password have NOT been changed.
        $user = new user(3);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertEquals($testPwd, $user->get("password"));

        user::setCurrent(new user(1));
    }
    /**
     * Test display by non-admin user
     */
    public function testDisplayNotAuthorized() {
        $this->expectException(securityException::class);
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "display",
                "user_id"      => 3,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
    }

    /**
     * Test change someone else's password by admin user
     */
    public function testUserPasswordAdmin() {
        user::setCurrent(new user(1));

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "password",
                "user_id"       => 3,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for jimi", $view->getTitle());
        $this->assertStringContainsString("<div class=\"messageText\">Make sure password and confirm match</div>", (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"3\">", (string) $template);
    }

    /**
     * Test change someone else's password by non-admin user
     * should ignore the user_id and just present the own password
     */
    public function testUserOtherPassword() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "password",
                "user_id"       => 3,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for freddie", $view->getTitle());
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"5\">", (string) $template);

        user::setCurrent(new user(1));
    }

    /**
     * Test change own password by non-admin user
     */
    public function testUserPassword() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "password",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $view=$controller->getView();
        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for freddie", $view->getTitle());
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"5\">", (string) $template);

        user::setCurrent(new user(1));
    }

    /**
     * Test change password by non-admin user - Update action
     */
    public function testPasswordNotAdminUpdate() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "_password"      => "changed"
            ),
            "SERVER" => array()
        ));

        $testUser = new user(5);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $controller = new controller($request);


        $view=$controller->getView();
        $this->assertInstanceOf(user\view\password::class, $view);

        // Verify password has been changed.
        $user = new user(5);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

        user::setCurrent(new user(1));
    }

    /**
     * Test change own password by admin user - Update action
     */
    public function testPasswordAdminUpdate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "_password"      => "changed"
            ),
            "SERVER" => array()
        ));

        $testUser = new user(1);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $controller = new controller($request);


        $view=$controller->getView();
        $this->assertInstanceOf(user\view\update::class, $view);

        // Verify password has been changed.
        $user = new user(1);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

        $user->set("password", validator::hashPassword("admin"));
        $user->update();
    }

    /**
     * Test change someone else's password by admin user - Update action
     */
    public function testUserPasswordAdminUpdate() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "_password"     => "changed",
                "user_id"       => 3,
            ),
            "SERVER" => array()
        ));

        $testUser = new user(3);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $controller = new controller($request);


        $view=$controller->getView();
        $this->assertInstanceOf(user\view\update::class, $view);

        // Verify password has been changed.
        $user = new user(3);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

    }

    public function getActions() {
        return array(
            array("new", user\view\update::class),
            array("edit", user\view\update::class),
            array("delete", user\view\confirm::class),
            array("users", user\view\users::class),
            array("nonexistant", user\view\display::class)
        );
    }
}
