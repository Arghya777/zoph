<?php
/**
 * Database migrations for v0.9.17
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use db\alter;
use db\column;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

class zophtest999 extends migration {

    protected const DESC="Test v99.99.99.999";
    protected const FROM="v99.99.99.998";
    protected const TO="v99.99.99.999";

    public function __construct() {
        $qry = new alter("test_v998");
        $qry->addColumn((new column("test"))->varchar(30));
        $this->addStep(self::UP, $qry, "Add column test");

        $qry = new alter("test_v998");
        $qry->dropColumn("test");
        $this->addStep(self::DOWN, $qry, "Drop column test");
    }
}

return new zophtest999();
