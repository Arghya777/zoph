<?php
/**
 * Database migrations for v0.9.17
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use db\column;
use db\drop;
use db\create;
use db\insert;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

class zophtest998 extends migration {

    protected const DESC="Test v99.99.99.998";
    protected const FROM="v99.99.99.997";
    protected const TO="v99.99.99.998";

    public function __construct() {
        $create = new create("test_v998");
        $create->addColumns(array(
            (new column("test_id"))->int()->setPrimaryKey()->autoincrement(),
            (new column("test_char"))->varchar(30)
        ));

        $this->addStep(self::UP, $create, "CREATE TABLE testv998");
        $this->addStep(self::DOWN, new drop("test_v998"), "DROP TABLE test_v998");
    }

    /**
     * Add a step where the 'direction' is not set to 'self::UP' or 'self::DOWN'...
     */
    public function addSidewaysStep() {
        $this->addStep(3, new insert(array("test_v998")), "Let's try a step sideways!");
    }
}

return new zophtest998();
