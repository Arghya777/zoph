<?php
/**
 * Set controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use set\controller;
use set\model as set;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the set controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class setControllerTest extends TestCase {

    protected function setUp() : void {
        conf::set("feature.sets", true)->update();
        user::setCurrent(new user(1));
    }

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }


    /**
     * Create set in the db
     */
    public function testInsertAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "insert",
                "name"         => "Test set"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $set=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\update::class, $view);
        $this->assertEquals("Test set", $set->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("set.php?_action=delete&amp;set_id=" . $set->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"set_id\" value=\"" . $set->getId() . "\">", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);

        return $set;
    }

    /**
     * Test the "sets" action - displays all sets
     * @depends testInsertAction
     */
    public function testSetsAction(set $set) {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "sets",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $setId = $set->getId();
        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\sets::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("new", (string) $template);
        $this->assertStringContainsString("set.php?_action=new", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);
        $this->assertStringContainsString("set.php?set_id=" . $setId, (string) $template);
        return $set;
    }

    /**
     * Test the "display" action
     * @depends testSetsAction
     */
    public function testDisplayAction(set $set) {
        $setId = $set->getId();
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "set_id"  => $setId
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete", (string) $template);
        $this->assertStringContainsString("Test set", (string) $template);
        $this->assertStringContainsString("set.php?_action=delete&set_id=" . $setId, (string) $template);
        return $set;
    }

    /**
     * Test the "addphoto" action
     * @depends testSetsAction
     */
    public function testAddphotoAction(set $set) {
        $setId = $set->getId();
        $request=new request(array(
            "GET"   => array(
                "_action"   => "addphoto",
                "set_id"    => $setId,
                "_photo_id" => 1
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([1], $photoIds);

        return $set;
    }

    /**
     * Test the "display" action - unauthorised
     * @depends testDisplayAction
     */
    public function testUnauthorisedDisplayAction(set $set) {
        $setId = $set->getId();
        user::setCurrent(new user(6));
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "set_id"  => $setId
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\notfound::class, $view);

    }

    /**
     * Test the "delete" action
     * @depends testUpdateAction
     */
    public function testDeleteAction(set $set) {
        $setId = $set->getId();
        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "set_id"  => $setId
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete set", (string) $template);
        $this->assertStringContainsString("set.php?_action=confirm&amp;set_id=" . $setId, (string) $template);
    }

    /**
     * Update set in the db
     * @depends testDisplayAction
     */
    public function testUpdateAction(set $set) {
        $setId = $set->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "set_id"      => $setId,
                "name"         => "Updated Set",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $set=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\update::class, $view);

        $this->assertEquals("Updated Set", $set->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("set.php?set_id=" . $setId, (string) $template);
        $this->assertStringContainsString("Updated Set", (string) $template);
        $this->assertStringNotContainsString("Test set", (string) $template);

        return $set;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "new",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $set=$controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals(0, $set->getId());

        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("set.php?_action=sets", (string) $template);
        $this->assertStringContainsString("new set", (string) $template);
        $this->assertStringContainsString("new set", $view->getTitle());
    }


    /**
     * Test create set coverphoto
     * @depends testInsertAction
     */
    public function testCoverPhotoAction(set $set) {
        $set->lookup();
        $setId = $set->getId();
        $this->assertEquals($set->get("coverphoto"), null);
        unset($set);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"    => "coverphoto",
                "set_id"   => $setId,
                "coverphoto" => "1",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $set=$controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals($set->get("coverphoto"), 1);

        $view=$controller->getView();
        $this->assertInstanceOf(\set\view\display::class, $view);

        return $set;
    }

    /**
     * Test unset coverphoto action
     * @depends testCoverPhotoAction
     */
    public function testUnsetCoverphotoAction(set $set) {
        $setId=$set->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "unsetcoverphoto",
                "set_id"      => $setId,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $set=$controller->getObject();
        $this->assertInstanceOf(set::class, $set);
        $this->assertEquals($set->get("coverphoto"), null);
        return $set;
    }

    /**
     * Test choose action
     * @depends testUnsetCoverphotoAction
     */
    public function testChooseAction(set $set) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "choose",
                "album_id"      => 5
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $this->assertInstanceOf(\set\view\choose::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"album_id\" value=\"5\">", (string) $template);
        $this->assertStringContainsString("Add photos to set", $view->getTitle());

        return $set;
    }

    /**
     * Test addphotos action
     * @depends testChooseAction
     */
    public function testAddPhotosAction(set $set) {
        $setId=$set->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "addphotos",
                "_set_id"        => $setId,
                "album_id"      => 5
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();

        $set=$controller->getObject();

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        foreach ([3,4,5] as $id) {
            $this->assertContains($id, $photoIds);
        }

        return $set;
    }

    /**
     * Test addphotos action
     * @depends testAddPhotosAction
     */
    public function testRemovePhotoAction(set $set) {
        $setId=$set->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "removephoto",
                "set_id"        => $setId,
                "photo_id"      => 3,
                "_qs"           => "set_id=" . $setId . "&amp;_order=set_order"
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: photos.php?set_id=" . $setId . "&_order=set_order"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $set;
    }

    /**
     * Test confirm (delete) action
     * @depends testRemovePhotoAction
     */
    public function testConfirmAction(set $set) {
        $setId=$set->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "confirm",
                "set_id"      => $setId,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $sets=set::getAll();
        $ids=array();
        foreach ($sets as $set) {
            $ids[]=$set->getId();
        }
        $this->assertNotContains($setId, $ids);

        $set=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: set.php?_action=sets"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    public function testMoveAction() {
        $set=new set();
        $set->set("name", "testMoveAction");
        $set->insert();

        $photoIds = array(6,7,8);

        foreach ($photoIds as $id) {
            $set->addPhoto(new photo($id));
        }

        $setId=$set->getId();

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "move",
                "set_id"        => $setId,
                "_photoId"      => 6,
                "_targetId"     => 8
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $set=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(\photos\view\json::class, $view);
        $json = $view->view();
        $this->assertEquals(array(7,6,8), json_decode($json));

        $set->delete();
    }
    public function getActions() {
        return array(
            array("display", \set\view\notfound::class),
            array("new", \set\view\update::class),
            array("sets", \set\view\sets::class),
            array("nonexistant", \set\view\notfound::class)
        );
    }
}
