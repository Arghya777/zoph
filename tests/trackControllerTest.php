<?php
/**
 * Comment controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use geo\controller;
use geo\track;
use PHPUnit\Framework\TestCase;
use web\request;

/**
 * Test the track controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class trackControllerTest extends TestCase {

    public static function tearDownAfterClass() : void {
        user::setCurrent(new user(1));
    }


    /**
     * Test the "new", "display", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $request=new request(array(
            "GET"   => array("_action" => $action),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $this->assertInstanceOf($expView, $controller->getView());
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $track = new track();
        $track->set("name", "Track to be kept");
        $track->insert();
        $id = $track->getId();
        
        $request=new request(array(
            "GET"   => array(
                "_action"   => "display",
                "track_id"  => $id
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(geo\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Track to be kept", (string) $template);
        $this->assertStringContainsString("track.php?_action=tracks", (string) $template);
        $this->assertStringContainsString("track.php?_action=edit&amp;track_id=" . $id, (string) $template);
        $this->assertStringContainsString("track.php?_action=delete&amp;track_id=" . $id, (string) $template);
        return $track;
    }

    /**
     * Test the "tracks" action
     * @depends testDisplayAction
     */
    public function testTracksAction(track $track) {
        $request=new request(array(
            "GET"   => array(
                "_action"   => "tracks",
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(geo\view\tracks::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Track to be kept", (string) $template);
        $this->assertStringContainsString("Tracks", (string) $template);
        $this->assertStringContainsString("track.php?track_id=" . $track->getId(), (string) $template);
        return $track;
    }

    /**
     * Update track in the db
     * @depends testTracksAction
     */
    public function testUpdateAction(track $track) {
        $id = $track->getId();
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "update",
                "track_id"      => $id,
                "name"          => "Track to be deleted",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $track=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(geo\view\update::class, $view);

        $this->assertEquals("Track to be deleted", $track->get("name"));

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Track to be deleted", (string) $template);
        $this->assertStringNotContainsString("Track to be kept", (string) $template);
        $this->assertStringContainsString("track.php?_action=display&amp;track_id=" . $track->getId(), (string) $template);

        return $track;
    }

    /**
     * Test the "delete" action
     * @depends testUpdateAction
     */
    public function testDeleteAction(track $track) {
        $id = $track->getId();

        $request=new request(array(
            "GET"   => array(
                "_action"   => "delete",
                "track_id"  => $id
            ),
            "POST"  => array(),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(geo\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm deletion of 'Track to be deleted'", (string) $template);
        $this->assertStringContainsString("track.php?_action=confirm&amp;track_id=" . $id, (string) $template);
        return $track;
    }

    /**
     * Delete track
     * @depends testDeleteAction
     */
    public function testConfirmAction(track $track) {
        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "confirm",
                "track_id"    => $track->getId(),
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);

        $track=$controller->getObject();

        $view=$controller->getView();
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: track.php?_action=tracks"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    /**
     * Test displaying all tracks by unauthorised user
     */
    public function testTracksNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "tracks",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    /**
     * Test displaying a track by unauthorised user
     */
    public function testDisplayNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();
        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "display",
                "track_id"    => 1,
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view = $controller->getView();
        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }
    
    /**
     * Test displaying all tracks by authorised user
     */
    public function testTracksNonAdminAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();
        $restore = clone $unauthUser;
        $unauthUser->set("browse_tracks", "1");
        $unauthUser->update();

        user::setCurrent($unauthUser);

        $request=new request(array(
            "GET"   => array(),
            "POST"  => array(
                "_action"       => "tracks",
            ),
            "SERVER" => array()
        ));

        $controller = new controller($request);
        $view=$controller->getView();
        $this->assertInstanceOf(geo\view\tracks::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Tracks", (string) $template);

        $restore->update();
    }

    public function getActions() {
        return array(
            array("edit", geo\view\update::class),
            array("delete", geo\view\confirm::class),
            array("display", geo\view\display::class),
            array("tracks", geo\view\tracks::class),
            array("nonexistant", geo\view\display::class)
        );
    }
}
