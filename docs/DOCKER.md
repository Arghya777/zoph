Zoph Organises Photos
=====================
![Zoph welcome screen](http://www.zoph.org/img/posts/new-welcome-screen.png)

![Latest Release](https://gitlab.com/zoph-project/zoph/-/badges/release.svg)

http://www.zoph.org

https://gitlab.com/zoph-project/zoph/

Zoph (Zoph Organises Photos) is a web based digital image presentation and management system. In other words, a photo album.

Many people store their photos in the digital equivalent of a shoe box: lots of directories with names like 'Holiday 2008', 'January 2005' or even 'Photos034'. Like shoe boxes, this is a great way to put your photos away, but not such a great way to find them back or even look at them. Zoph can help you to store your photos and keep them organised.

While most photo album projects are primarily targeted at showing your photos to others, Zoph is primarily targeted at keeping your photos organized for yourself, giving you granular control over what you'd like to show to others, on a per-album or even a per-photo basis.

Zoph's Docker images are published on [Docker Hub](https://hub.docker.com/r/zophproject/zoph) and [Gitlab](https://gitlab.com/zoph-project/zoph/container_registry).

How to use this image
---------------------

### Using Gitlab's repo ###
```
docker run --name zoph -e TZ="Europe/Amsterdam" -e DB_PASS="T0pS3cret" -d registry.gitlab.com/zoph-project/zoph:stable -v "/data/images:data/images"
```
### Using Docker Hub repo ###
```
docker run --name zoph -e TZ="Europe/Amsterdam" -e DB_PASS="T0pS3cret" -d zophproject/zoph:stable -v "/data/images:data/images"
```

You will need to provide a MySQL (MariaDB) server.

### Environment variables ###
| NAME          | PURPOSE                               | EXAMPLE           | DEFAULT |  REQUIRED  |
|--------------:|---------------------------------------|-------------------|---------|:----------:|
|`TZ`           | Set timezone                          |Europe/Amsterdam   | -       | yes        |
|`DB_NAME`      | Set the name of the MySQL database    |zoph               | zoph    | no         |
|`DB_HOST`      | Set the name or IP of the MySQL server|10.0.0.1           | sql     | no         |
|`DB_PASS`      | Password for MySQL database           |S3cr3t             | -       | yes        |
|`DB_USER`      | User to use to login to MySQL         |zoph_rw            | zoph    | no         |

### Volumes ###
Store your photos in a persistent volume. If you don't and upload images to Zoph, they will be gone after restarting the container!

You can choose the location where you mount this. Zoph's default is `/data/images`, if you choose another location, you will need to modify it trough Admin -> Config -> Paths -> Image path.

Tags
----

| NAME              | USAGE                                                                                 |
|-------------------|---------------------------------------------------------------------------------------|
| vx.x              | Stable release, pinned, you need to manually change to upgrade                        |
| stable            | Latest stable release, automatic update                                               |
| latest            | Latest stable release, automatic update (identical to stable)                         |
| develop           | Latest development release, recommended if you are hit by a bug in stable             |
| issue-xxx         | Issue release, use only if you would like to check out a specific feature or bug fix  |
