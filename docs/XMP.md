# XMP Support #

As of Zoph v0.9.15, Zoph has basic XMP support. This will be expanded over the next releases. If you have suggestions for improvements, please [let me know](#13). Also, if you have examples of images you added XMP to that are not handled by Zoph as you expect, feel free to send them to me (preferably via [Issue#13](#13).

## What is XMP ##
**XMP** stands for *eXtensible Metadata Platform* and is an ISO standard to record Metadata for various file types, either embedded in the file or added to a separate file. For more information, see [Wikipedia](https://en.wikipedia.org/wiki/Extensible_Metadata_Platform) or [Adobe's site](https://www.adobe.com/products/xmp.html).

## What can be done with XMP ##
XMP is meant to share metadata between applications. So that, if you have assigned categories or people to a photo in one program, you can easily import that information in another program.

## What XMP support does Zoph currently have? ##
As of v0.9.15, Zoph has very basic XMP support. It will now recognize ["Dublin Core"](https://dublincore.org/) Subjects embedded in JPG files, and use them as **Categories** during import via the web interface.

If you use for example [Geeqie](http://geeqie.org/)[^1] to assign **Keywords** to a photo, these will be recorded as "Dublin Core Subjects" (dc:subject) and will be recognized by Zoph.

If Zoph finds *dc:subject* tags in a file you are importing via the web interface, it will display a small tag under each photo.

As of v0.9.16, Zoph will also process XMP ratings.

![XMP tags on web import](img/xmpImport.png)

The tag will either be **grey** or **red**. A **red** tag indicates that Zoph does not recognize this *subject* and cannot import it. A **grey** tag indicates that there is a **category** in Zoph's database that *exactly* matches the *subject* name and Zoph will assign this *category* to this photo during import.

As of v0.9.16, Zoph will also process XMP tags when importing via the CLI, provided you specify `--XMP`. Unknown subjects are silently ignored.

## What XMP support will Zoph have in the future ##

First and foremost, please let me know what **you** could use to improve your workflow with Zoph!

Furthermore, I am planning to add at least **Album**, **Person** and **Location** support to Zoph. Also, I expect that not all application will use the same XMP tags as Geeqie does, so I will try to add support for other applications in the future.

Finally, I am planning to make Zoph *write* XMP tags as well, so you can use the information you added in Zoph to feed other applications.

[^1]: If you have used other applications, succesfully or unsuccesfully, please let me know so I can update the documentation or improve Zoph with support for more applications.
