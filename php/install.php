<?php
/**
 * Zoph upgrade page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use db\db;
use db\select;
use db\exception as databaseException;
use conf\conf;
use install\controller;
use web\request;
use web\redirect;
use web\session;

define("INSTALL", true);

try {
    require_once "autoload.inc.php";
    require_once "exception.inc.php";
    require_once "config.inc.php";

    settings::$instance = "INSTALL";
    $session=new session();
    $session->start();
    if (isset($session["INSTALL"])) {
        throw new configurationException("Installation in progress");
    }
    db::query(new select("photos"));
    die("no configuration necessary");
} catch (configurationException | databaseException | PDOException $e) {
    settings::$php_loc = dirname($_SERVER['SCRIPT_FILENAME']);
    settings::$instance = "INSTALL";
    require_once "variables.inc.php";
    if (!isset($session)) {
        $session=new session();
        $session->start();
    }

    if (!isset($session["INSTALL"])) {
        $session["INSTALL"]=true;
    }
} catch (exception $e) {
    die("Unknown error: <b>" . get_class($e) . "</b><br>" . $e->getMessage());
}

$lang = new language("en");
$ctrl = new controller(request::create());
$view = $ctrl->getView();
$view->display();
