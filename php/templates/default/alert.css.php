<?php
/**
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 *
 */
use template\template;

?>

div.alert.error,
div.alert.warning,
div.alert.success {
    position: fixed;
    top: 20px;
    left: 40%;
    margin:    0px auto 20px auto;
    padding: 1em 2em 1em 100px;
    background: white;
    width: 20%;
    height: 4rem;
    border-radius: 15px;
    opacity: 0.85;
    box-shadow: 5px 5px 5px 5px rgba(0,0,0,0.5);
    font-size: 14px;
    font-weight: bold;
    text-align: center;
    color: black;
}

div.alert.error {
    background: url("<?= template::getImage("icons/error.png") ?>") no-repeat #ffbbbb 15px;
}

div.alert.warning {
    background: url("<?= template::getImage("icons/warning.png") ?>") no-repeat #ffffbb 15px;
}

div.alert.success {
    background: url("<?= template::getImage("icons/ok.png") ?>") no-repeat #bbffbb 15px;
}

div.alert.error:empty,
div.alert.warning:empty,
div.alert.success:empty {
    display: none;
}

/* vim: set syntax=css: */
