<?php
/**
 * Template for displaying a table of sets
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!ZOPH) { die("Illegal call"); }
?>
<?php if (empty($tpl_sets)): ?>
    <?= translate("Nothing found") ?>
    <?php return ?>
<?php endif ?>
<table class="sets">
    <caption><?= translate("sets") ?></caption>
    <tr>
        <th scope="col"><?= translate("title") ?></th>
        <th scope="col"><?= translate("number of photos") ?></th>
        <th scope="col"><?= translate("last modified") ?></th>
        <th scope="col"><?= translate("user") ?></th>
    </tr>
    <?php foreach ($tpl_sets as $set): ?>
        <tr>
            <td>
                <a href=set.php?set_id=<?= $set->getId() ?>>
                    <?= $set->getName() ?>
                </a>
            </td>
            <td>
                <?= $set->getPhotoCount() ?>
            </td>
            <td>
                <?= $set->get("timestamp") ?>
            </td>
            <td>
                <?= $set->getUser()->getLink() ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>
