<?php
/**
 * Template for displaying both photos when working on
 * a photo\relation.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}
?>
<div class="relation">
    <div class="thumbnail">
        <?= $tpl_photo1 ?><br>
        <?= $tpl_desc1 ?>
    </div>
    <div class="thumbnail">
        <?= $tpl_photo2 ?><br>
        <?= $tpl_desc2 ?>
    </div>
</div>
