<?php
/**
 * Template for the main page of Zoph
 *
 * This is a copy of main.tpl.php that will not send headers
 * this is actually the right way to do it, but currently
 * many parts of Zoph are not prepared for this
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */

if (!ZOPH) { die("Illegal call"); }
?>
<div class="alert error"><?= $tpl_error ?? "" ?></div>
<div class="alert warning"><?= $tpl_warning ?? "" ?></div>
<div class="alert success"><?= $tpl_success ?? "" ?></div>
<h1>
    <?= $this->getActionlinks(); ?>
    <?= $tpl_title; ?>
</h1>
<?= $tpl_selection ?? "" ?>
<div class="main" id="main">
    <?= $this->displayBlocks(); ?>
</div>

<?= $tpl_map ?? "" ?>
