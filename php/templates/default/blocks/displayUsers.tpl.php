<?php
/**
 * Template for displaying users
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}
?>
<h1>
    <?= $this->getActionlinks($tpl_actionlinks) ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <table class="users">
        <caption><?= translate("users") ?></caption>
        <tr>
            <th scope="col"><?= translate("username") ?></th>
            <th scope="col"><?= translate("name") ?></th>
            <th scope="col"><?= translate("last login") ?></th>
        </tr>

        <?php foreach ($tpl_users as $user): ?>
            <tr>
                <td>
                    <a href="<?= $user->url ?>">
                        <?= $user->name ?>
                    </a>
                </td>
                <td>
                    <?php if ($user->person): ?>
                        <a href="<?= $user->personURL ?>">
                            <?= $user->person; ?>
                        </a>
                    <?php endif ?>
                </td>
                <td>
                    <?= $this->getActionlinks($user->actionlinks) ?>
                    <?= $user->lastlogin ?>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
</div>

