<?php
/**
 * Template for the edit photo block on edit photos page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!ZOPH) {
    die("Illegal call");
}
?>
<fieldset class="editphotos">
    <legend>
        <?= $tpl_name ?>
    </legend>
    <fieldset class="editchoice">
        <legend><?= $tpl_name ?></legend>
        <input type="hidden" name="__photo_id__<?= $tpl_photoId ?>" value="<?= $tpl_photoId ?>">
        <input type="radio" name="_action__<?= $tpl_photoId ?>" value="update" checked><?= translate("edit", 0) ?><br>
        <input type="radio" name="_action__<?= $tpl_photoId ?>" value=""><?= translate("skip", 0) ?><br>
        <input type="radio" name="_action__<?= $tpl_photoId ?>" value="delete"><?= translate("delete", 0) ?>
    </fieldset>
    <div class="thumbnail">
        <?= $tpl_thumb ?>
    </div>
    <?= $this->displayBlocks(); ?>
</fieldset><br>

