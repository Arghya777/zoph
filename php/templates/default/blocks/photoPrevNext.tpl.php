<?php
/**
 * Template for next/prev buttons on photo page
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!ZOPH) { die("Illegal call"); }
?>

<nav class="photohdr">
    <ul>
        <?php if ($tpl_prev): ?>
            <li class="prev">
                <a href="<?= $tpl_prev ?>">
                    <?= translate("Prev") ?>
                </a>
            </li>
        <?php endif ?>
        <?php if ($tpl_up): ?>
            <li class="up">
                <a href="<?= $tpl_up ?>">
                    <?= translate("Up") ?>
                </a>
            </li>
        <?php endif ?>
        <?php if ($tpl_next): ?>
            <li class="next">
                <a href="<?= $tpl_next ?>">
                    <?= translate("Next") ?>
                </a>
            </li>
        <?php endif ?>
    </ul>
</nav>

