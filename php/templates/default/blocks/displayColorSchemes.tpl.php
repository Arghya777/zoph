<?php
/**
 * Template for displaying a list of colour schemes
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!ZOPH) {
    die("Illegal call");
}
?>
<h1>
    <?= $this->getActionlinks() ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <table class="colorschemes">
        <caption><?= $tpl_title ?></caption>
        <tr>
            <th scope="col"><?= translate("name") ?></th>
            <th scope="col"><?= translate("preview") ?></th>
            <th scope="col"><?= translate("options") ?></th>
        </tr>
        <?php foreach ($tpl_cs as $cs): ?>
            <tr>
                <td><?= $cs->get("name") ?></td>
                <td>
                    <?php foreach ($cs->getColors() as $name=>$color): ?>
                        <div class="cs_example" style="background: #<?= $color ?>"></div>
                    <?php endforeach ?>
                </td>
                <td>
                    <ul class="actionlink">
                        <li>
                            <a href="color_scheme.php?_action=delete&amp;color_scheme_id=<?=$cs->getId() ?>">
                                <?= translate("delete") ?>
                            </a>
                        </li>
                        <li>
                            <a href="color_scheme.php?_action=edit&amp;color_scheme_id=<?= $cs->getId() ?>">
                                <?php echo translate("edit") ?>
                            </a>
                        </li>
                        <li>
                            <a href="color_scheme.php?_action=copy&amp;color_scheme_id=<?= $cs->getId() ?>">
                                <?php echo translate("copy") ?>
                            </a>
                        </li>
                    </ul>
                </td>
            </tr>
        <?php endforeach ?>
</div>
