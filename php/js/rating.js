// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zRating=function() {
    function rating(rating, id) {
        let div = document.createElement("div");
        div.className = "rating";
        div.id = id;
        div.title = rating;
        div.dataset.rating = rating;
        let stars = document.createElement("div");
        stars.className = "stars";
        stars.style.width=rating*10 + "%";
        div.appendChild(stars);
        return(div);
    }

    function ratingButton(inputId, dropdownId) {
        let input = document.getElementById(inputId);
        let dropdown = document.getElementById(dropdownId);
        
        input.disabled = (dropdown.value == "0");
    }

    return {
        rating:rating,
        ratingButton:ratingButton
    };
}();
