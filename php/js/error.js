
// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zError=function() {
    var errors = [];

    function push(error) {
        zError.errors.push(error);
        zError.update();
    }

    function update() {
        var ulError = document.getElementById("errors");
        if(!ulError) {
            ulError = document.createElement("ul");
            ulError.id = "errors";
            document.body.insertBefore(ulError, document.body.firstChild);
        }
        while(error=zError.errors.pop()) {
            var liError = document.createElement("li");
            liError.id = "error_" + (((1+Math.random())*0x10000)|0).toString(16);
            setTimeout(zError.animateError.bind(null, liError.id), 5000);
            setTimeout(zError.deleteError.bind(null, liError.id), 7000);

            liError.innerHTML = error;
            ulError.insertBefore(liError, ulError.firstChild);
        }
    }
    
    function animateError(id) {
        var li=document.getElementById(id);
        li.className="deleted";
    }

    function deleteError(id) {
        var li=document.getElementById(id);
        li.parentElement.removeChild(li);
    }

    return {
        push:push,
        update:update,
        errors:errors,
        animateError:animateError,
        deleteError:deleteError
    };
}();
