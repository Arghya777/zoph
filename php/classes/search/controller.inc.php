<?php
/**
 * Controller for searches
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace search;

use generic\controller as genericController;
use photo\collection;
use photos\params;
use search;
use web\request;
use user;

/**
 * Controller for searches
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewPhotos    = \photos\view\display::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array("confirm", "delete", "display", "edit", "insert", "new", "update", "search");

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {

        // Because of the "+" buttons on the search form, the actual search button
        // is named "_action", however, this causes the _action to be translated
        // this is a bit of a hack and should be fixed by changing the "+" buttons
        // to Javascript buttons
        if ($request["_action"] == translate("search")) {
            $request["_action"] = "search";
        }
        $_action = $request["_action"];
        parent::__construct($request);
        if (isset($this->request["search_id"])) {
            $search = new search($this->request["search_id"]);
            $search->lookup();
        } else if ($_action=="new") {
            $search=new search();
            $search->setSearchURL($this->request);
            $search->set("owner", user::getCurrent()->getId());
        } else {
            $search=new search();
        }
        $this->setObject($search);
        $this->doAction();
    }

    /**
     * Do action 'search'
     */
    public function actionSearch() {
        $photos = collection::createFromRequest($this->request);
        $params = new params($this->request);
        $display = $photos->subset($params->offset, $params->cells);
        $lightbox = $params->getLightBox();

        $this->view=new static::$viewPhotos($this->request, $params);
        $this->view->setPhotos($photos);
        $this->view->setDisplay($display);
        $this->view->setLightBox($lightbox);
        $style = $params->getStyle();
        if ($style) {
            $this->view->setStyle($style);
        }
    }
}
