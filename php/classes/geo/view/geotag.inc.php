<?php
/**
 * View to display the form to start geotagging
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace geo\view;

use conf\conf;
use geo\map;
use geo\track;
use template\block;
use template\template;
use photo\collection;
use user;
use web\request;

/**
 * This view displays a form to start geotagging
 */
class geotag extends view implements \view {
    
    public function __construct(protected request $request) {
    }

    /**
     * Output view
     */
    public function view() : block {
        $photos=collection::createFromRequest($this->request);
        $photoCount = sizeof($photos);

        $hidden=$this->request->getRequestVarsClean();
        unset($hidden["_off"]);
        $hidden["_action"]="dogeotag";

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(new block("geotag_form", array(
            "photoCount"    => $photoCount,
            "hidden"        => $hidden,
            "tracks"        => track::getRecords("track_id")
        )));


        return $tpl;
    }
    
    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }
    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Geotag");
    }
}
