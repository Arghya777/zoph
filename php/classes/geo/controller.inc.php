<?php
/**
 * Controller for tracks
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace geo;

use conf\conf;
use generic\controller as genericController;
use photo\collection;
use web\request;
use photo;
use user;

use photoNotAccessibleSecurityException;
use securityException;
use userInsufficientRightsSecurityException;

/**
 * Controller for tracks
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewGeotag    = view\geotag::class;
    protected static $viewDoGeotag  = view\dogeotag::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewTracks    = view\tracks::class;
    protected static $viewUpdate    = view\update::class;

    protected $objects = array();

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "dogeotag", "edit", "geotag", "tracks", "update"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        try {
            $user=user::getCurrent();
            if (!$user->canBrowseTracks()) {
                throw new userInsufficientRightsSecurityException("User has no rights to browse tracks");
            }

            if ($request["_action"] == "tracks") {
                $this->objects=track::getRecords();
            } else {
                $this->object = $this->getTrack();
                
            }

            $this->doAction();
        } catch (securityException $e) {
            $this->actionForbidden($e);
        }

    }

    private function getTrack() {
        $track_id = (int) $this->request["track_id"];
        $track = new track($track_id);
        if ($track_id !== 0) {
            $track->lookup();
        }
        return $track;
    }

    protected function actionTracks() {
        $this->view = new static::$viewTracks($this->request, $this->objects);
    }

    /**
     * Do action 'confirm'
     */
    protected function actionConfirm() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionConfirm();
            $this->view->setRedirect("track.php?_action=tracks");
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    protected function actionDelete() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'edit'
     */
    protected function actionEdit() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'geotag'
     * this shows the form to start the geotagging process
     */
    protected function actionGeotag() : void {
        $user = user::getCurrent();
        if (!$user->isAdmin()) {
            $this->view = new static::$viewDisplay($this->request, $this->object);
            return;
        }
        $photos=collection::createFromRequest($this->request);
        $photoCount = sizeof($photos);

        if ($photoCount == 0) {
            $this->view = new static::$viewNotfound($this->request);
        } else {
            $this->view = new static::$viewGeotag($this->request);
        }
    }
    
    /**
     * Do action 'dogeotag'
     * this does the test of the geo tag and displays the results, as well as the actual geotagging + results
     */
    protected function actionDoGeotag() : void {
        $user = user::getCurrent();
        if (!$user->isAdmin()) {
            $this->view = new static::$viewDisplay($this->request, $this->object);
            return;
        }
        $this->view     = new static::$viewDoGeotag($this->request);

        $validtz        = (bool) $this->request["_validtz"];
        $overwrite      = (bool) $this->request["_overwrite"];
        $count          = (int) $this->request["_testcount"];
        $tracks         = $this->request["_tracks"];
        $maxtime        = $this->request["_maxtime"];
        $interpolate    = $this->request["_interpolate"];
        $int_maxdist    = $this->request["_int_maxdist"];
        $int_maxtime    = $this->request["_int_maxtime"];
        $entity         = $this->request["_entity"];
        $test           = $this->request["_test"];
        $tphotos        = array();
        $photos         = collection::createFromRequest($this->request);

        $new_vars       = $this->request->getUpdatedVars("_action", "dogeotag", array("_test", "_testcount"), clean: false);

        if ($tracks != "all") {
            $track = new track($this->request["_track"]);
            $track->lookup();
        } else {
            $track=null;
        }

        if ($validtz) {
            $photos->removeNoValidTZ();
        }
        
        if (!$overwrite) {
            $photos->removeWithLatLon();
        }

        $total = count($photos);
        if ($total>0) {
            if (is_array($test)) {
                $photos=$photos->getSubsetForGeotagging($test, $count);
            }
            foreach ($photos as $photo) {
                $point = $photo->getLatLon($track, $maxtime, $interpolate, $int_maxdist, $entity, $int_maxtime);
                if ($point instanceof point) {
                    $photo->setLatLon($point);
                    if (!is_array($test)) {
                        $photo->update();
                    }
                    $tphotos[]=$photo;
                }
            }

            $tagged=count($tphotos);
            $map=new map();
            $map->addMarkers($tphotos, $user);

        } else {
            $tagged=0;
        }
        $this->view->url          = "track.php?" . http_build_query($new_vars);
        $this->view->isTest       = (bool) is_array($test);
        $this->view->taggedCount  = (int) $tagged;
        $this->view->totalCount   = (int) $total;
        $this->view->map          = $map ?? null;

    }
        
    /**
     * Do action 'update'
     */
    protected function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionUpdate();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

}
