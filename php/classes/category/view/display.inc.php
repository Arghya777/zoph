<?php
/**
 * View to display categories
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use conf\conf;
use category;
use pageException;
use photoNoSelectionException;
use selection;
use template\block;
use user;
use web\request;
use organiserView;

/**
 * Display screen for categoriess
 */
class display extends view implements \view {
    use organiserView;

    private const VIEWNAME = "Category View";
    private const PHOTOLINK = "photos.php?category_id=";
    private const DESCRIPTION = "category_description";

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();
        $actionlinks=array();

        if ($user->canEditOrganizers()) {
            $actionlinks=array(
                translate("edit") => "category.php?_action=edit&amp;category_id=" . (int) $this->object->getId(),
                translate("new") => "category.php?_action=new&amp;parent_category_id=" . (int) $this->object->getId(),
                translate("delete") => "category.php?_action=delete&amp;category_id=" . (int) $this->object->getId()
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks["unset coverphoto"]="category.php?_action=unsetcoverphoto&amp;category_id=" . (int) $this->object->getId();
            }
        }
        return $actionlinks;
    }

    private function getSelection() : ?selection {
        try {
            $selection=new selection($_SESSION, array(
                "coverphoto"    => "category.php?_action=coverphoto&amp;category_id=" . $this->object->getId() . "&amp;coverphoto=",
                "return"        => "_return=category.php&amp;_qscategory_id=" . $this->object->getId()
            ));
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }

        return $selection;    
    }
    
    public function getTitle() : string {
        return $this->object->get("parent_category_id") ? $this->object->get("category") : translate("Categories");
    }
}
