<?php
/**
 * View for edit category page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use conf\conf;
use template\template;
use template\block;
use web\request;
use permissions\view\edit as editPermissions;

use category;
use user;

/**
 * This view displays the category page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        if ($this->request["category_id"] != 0) {
            $returnURL = "category.php?category_id=" . (int) $this->request["category_id"];
        } else if ($this->object->getId() != 0) {
            $returnURL = "category.php?category_id=" . (int) $this->object->getId();
        } else if ($this->request["parent_category_id"] != 0) {
            $returnURL = "category.php?category_id=" . (int) $this->request["parent_category_id"];
        } else {
            $returnURL = "category.php";
        }

        if ($this->request["_action"] == "new") {
            return array(
                translate("return")    => $returnURL
            );
        } else {
            return array(
                translate("return")    => $returnURL,
                translate("new")       => "category.php?_action=new&amp;parent_category_id=" . $this->object->getId(),
                translate("delete")    => "category.php?_action=delete&amp;category_id=" . $this->object->getId()
            );
        }
    }

    /**
     * Output the view
     */
    public function view() : block {
        $user = user::getCurrent();

        $actionlinks=$this->getActionlinks();

        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
            $permissions="";
        }

        return new block("editCategory", array(
            "actionlinks"   => $actionlinks,
            "action"        => $action,
            "category"      => $this->object,
            "title"         => $this->getTitle(),
            "user"          => $user,
        ));
    }

}
