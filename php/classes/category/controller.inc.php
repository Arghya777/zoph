<?php
/**
 * Controller for category
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category;

use conf\conf;
use generic\controller as genericController;
use web\request;

use breadcrumb;
use log;
use category;
use rating;
use user;

use categoryNotAccessibleSecurityException;

use organiserActions;

/**
 * Controller for category
 */
class controller extends genericController {
    use organiserActions;

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "new", "insert", "confirm", "delete", "display", "edit", "update", "coverphoto", "unsetcoverphoto"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if ($request->_action=="new") {
            $category=new category();
            if (isset($this->request["parent_category_id"])) {
                $category->set("parent_category_id", $this->request["parent_category_id"]);
            }
            $this->setObject($category);
            $this->doAction();
        } else {
            try {
                $category=$this->getCategoryFromRequest();
            } catch (categoryNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $category=null;
            }
            if ($category instanceof category) {
                $this->setObject($category);
                $this->doAction();
            } else {
                $this->view = new static::$viewNotfound($this->request, new category());
            }
        }
    }

    /**
     * Get the category based on the query in the request
     * @throws categoryNotAccessibleSecurityException
     */
    private function getCategoryFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["category_id"])) {
            $category = new category( (int) $this->request["category_id"]);
            $category->lookup();
        } else {
            $category=category::getRoot();
        }
        if ($user->canSeeAllPhotos() || $category->getCountForUser() > 0) {
            return $category;
        }
        throw new categoryNotAccessibleSecurityException(
            "Security Exception: category " . (int) $this->request["category_id"] .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->canDeletePhotos()) {
            $parent = $this->object->get("parent_category_id");
            parent::actionConfirm();
            $this->view->setRedirect("category.php?category_id=" . $parent);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionDelete();
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'new'
     */
    public function actionInsert() {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

    /**
     * Do action 'new'
     */
    public function actionNew() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }
}
