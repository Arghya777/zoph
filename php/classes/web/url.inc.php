<?php
/**
 * The url object represents an http URL
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web;

use conf\conf;

/**
 * The url object represents an http URL
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class url {

    /**
     * Get Zoph's URL
     */
    public static function get() {
        $override = conf::get("share.url.override");
        if (!empty($override)) {
            if (substr($override, -1) != "/") {
                $override .= "/";
            }
            return $override;
        }

        $request = request::create();
        $https = $request->getServerVar("HTTPS");
        if ($https && !empty($https) && $https != "off") {
            $proto = "https";
        } else {
            $proto = "http";
        }

        $current = $request->getServerVar("SERVER_NAME") . "/" . $request->getServerVar("PHP_SELF");
        $dirname = substr($current, 0, strrpos($current, "/") + 1);
        return $proto . "://" . preg_replace("/\/\//", "/", $dirname);
    }
}
