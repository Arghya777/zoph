<?php
/**
 * Controller for web services
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\service;

use album;
use conf\conf;
use feature\base as feature;
use generic\controller as genericController;
use geo\locationLookup;
use person;
use photo;
use photo\collection;
use photo\data as photoData;
use photo\people as photoPeople;
use user;
use web\request;

/**
 * Controller for web services
 */
class controller extends genericController {
    protected static $viewJson = view\json::class;

    /** @var array Actions that can be used in this controller */
    protected $actions = array(
        "locationLookup",
        "photoData",
        "photoPeople",
        "search",
        "translation",
        "feature"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        $this->request=$request;
        $this->doAction();
    }

    /**
     * Do action 'locationLookup'
     */
    public function actionLocationlookup() {
        $search = $this->request["search"];
        $server = $this->request->getServerVar("SERVER_NAME");
        $location = new locationLookup($search, $server);
        $data = [ "search" => $search, "lat" => $location->getLat(), "lon" => $location->getLong(), "zoom" => $location->getZoom() ];
        $this->view = new static::$viewJson($this->request, $data);
    }

    /**
     * Do action 'photoData'
     */
    public function actionPhotoData() {
        $this->view = "json";
        $photoId = $this->request["photoId"];
        $photo = new photo($photoId);
        if ($photo->lookup()) {
            $photodata = new photoData($photo);
            $data = $photodata->getData();
        }
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }

    /**
     * Do action 'photoPeople'
     */
    public function actionPhotoPeople() {
        $this->view = "json";
        $photoId = (int) $this->request["photoId"];
        $personId = (int) $this->request["personId"];
        $photo = new photo($photoId);
        $person = new person($personId);
        $photoPeople = new photoPeople($photo);
        $user=user::getCurrent();

        if (!$photo->isWritableBy($user)) {
            return;
        }

        $action = $this->request["action"];
        if (in_array($action, [ "left", "right", "up", "down" ])) {
            list($row, $pos) = $photoPeople->getPosition($person);
        }
        switch($action) {
            case "left":
                $photoPeople->setPosition($person, $pos - 1);
                break;
            case "right":
                $photoPeople->setPosition($person, $pos + 1);
                break;
            case "up":
                $photoPeople->setRow($person, $row - 1);
                break;
            case "down":
                $photoPeople->setRow($person, $row + 1);
                break;
            case "add":
                $row = (int) $this->request["row"];
                $person->addPhoto($photo);
                $photoPeople->setRow($person, $row);
                break;
            case "remove":
                $person->removePhoto($photo);
                break;
            default:
                // Not making any changes, just outputting status quo
                break;
        }
        if ($photo->lookup()) {
            $photoPeople = new photoPeople($photo);
            $data = $photoPeople->getData();
        }
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }

    public function actionSearch() {
        $photoCollection = collection::createFromRequest($this->request);

        $data=$photoCollection->getIds();
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }

    public function actionTranslation() {
        global $lang;
        $this->view = new static::$viewJson($this->request, $lang->getAllTranslations());
    }

    public function actionFeature() {
        $type = $this->request["type"];
        $count = $this->request["count"] ?? 10;
        $features = feature::getAll();
        $feature = new $features[$type];
        $this->view = new static::$viewJson($this->request, $feature->getData($count));

    }
}
