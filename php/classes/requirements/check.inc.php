<?php
/**
 * Requirements class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements;

use conf\conf;
use configurationException;
use template\block;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class check {
    private $output = array();
    private $status = true;

    public function doChecks() {
        $checks = array(
            new requirement\fileinfo(),
            new requirement\phpVersion(),
            new requirement\magicFile(),
            new requirement\timezone(),
            new requirement\session(),
            new requirement\gd2(),
            new requirement\exif(),
            new requirement\domdocument(),
            new requirement\xmlreader(),
            new requirement\phpIniMaxInputTime(),
            new requirement\phpIniMaxExecutionTime(),
            new requirement\phpIniMemoryLimit(),
            new requirement\phpIniUploadMaxFilesize(),
            new requirement\phpIniPostMaxsize()
        );
        foreach ($checks as $check) {
            $check->doCheck();
            $this->output[] = (string) $check;
            $result = $check->getResult();
            if ($result === null) {
                // ignore warnings
                $result = true;
            }
            $this->status = $this->status && $result;
        }
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * Display the requirements overview
     */
    public function show() {
        return new block("requirements", array(
            "title"     => translate("Requirements", false),
            "results"   => $this->output
        ));
    }

}


