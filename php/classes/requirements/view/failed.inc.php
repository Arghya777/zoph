<?php
/**
 * View for display requirements check
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace requirements\view;

use conf\conf;
use template\block;
use template\page;
use web\request;
use web\view\view;
use requirements\check as checkRequirements;
/**
 * Check requirements
 */
class failed extends view implements \view {

    /** @var requirements */
    private $requirements;

    /**
     * View for requirements
     * @return template\block
     */
    public function view() : block {

        $text = $this->requirements->show();
        $text->addBlock(new block("message", array(
            "class"         => "error",
            "title"         => "unmet requirements",
            "text"       => "Not all requirements have been met. Zoph cannot run. " .
                "Please fix the issues above and retry."
        )));
        $text->addBlock(new block("button", array(
            "button"        => "retry",
            "button_class"  => "requirements",
            "button_url"    => $_SERVER["PHP_SELF"]
        )));

        $body = new block("main", array(
            "title" =>  $this->getTitle()
        ));
        $body->addBlock($text);
        return $body;
    }

    public function display($template = "html") : void {
        $page = new page($template, array(
            "iso"           => "en",
            "menu"          => null,
            "title"         => $this->getTitle(),
            "breadcrumbs"   => null
        ));
        $page->addBlock($this->view());

        echo $page;
    }

    public function addRequirements(checkRequirements $requirements) {
        $this->requirements = $requirements;
    }

    public function getTitle() : string {
        return "Failed requirements";
    }
}
