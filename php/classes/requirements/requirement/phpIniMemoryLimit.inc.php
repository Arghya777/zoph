<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class phpIniMemoryLimit extends phpIniCheck {

    protected const NAME = "php.ini: memory_limit";
    protected const DESCRIPTION = "Maximum amount of memory Zoph can use (esp. relevant for resizing large photos)";
    protected const MSG_PASS = "memory_limit sufficient.";
    protected const MSG_FAIL = "memory_limit should be 16M or more (32M or more recommended).";
    protected const MSG_WARNING = "memory_limit is set to less than 32M, this may not be sufficient for resizing large photos.";

    protected function check() {
        $mem = $this->toBytes(ini_get("memory_limit"));

        if ($mem === -1 ) {
            // -1 (unlimited) is ok
            return static::PASS;
        } else if ($mem < (16 * 1024 * 1024)) {
            return static::FAIL;
        } else if ($mem < (32 * 1024 * 1024)) {
            return static::WARNING;
        } else {
            return static::PASS;
        }
    }

}
?>
