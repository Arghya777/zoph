<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;
use configurationException;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class magicFile extends requirement {

    protected const NAME = "Magic file";
    protected const DESCRIPTION = "Check if magic file exists";
    protected const MSG_PASS = "Magic configuration valid";
    protected const MSG_FAIL = "The file for path.magic does not exist. Set the location of your " .
        "magic file in admin -> config to your MIME magic file.";
    protected const MSG_WARNING = "Check skipped during installation.";

    protected function check() {
        try {
            return (file_exists(conf::get("path.magic")) || strlen(conf::get("path.magic")) == 0);
        } catch (configurationException $e) {
            return static::WARNING;
        }

    }

}
?>
