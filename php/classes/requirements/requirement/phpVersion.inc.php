<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class phpVersion extends requirement {

    protected const NAME = "PHP version";
    protected const DESCRIPTION = "Check PHP version";
    protected const MSG_PASS = "PHP version supported.";
    protected const MSG_FAIL = "You should run at least PHP 8.0 to use Zoph";
    protected const MSG_WARNING = "Zoph has not yet been tested against version PHP 8.2 and later. Feel free to report a bug if you run into problems.";

    protected function check() {
        if (PHP_VERSION_ID < 80000) {
            return static::FAIL;
        } else if (PHP_VERSION_ID > 80200) {
            return static::WARNING;
        } else {
            return static::PASS;
        }
    }

}
?>
