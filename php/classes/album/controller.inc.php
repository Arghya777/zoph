<?php
/**
 * Controller for album
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album;

use conf\conf;
use generic\controller as genericController;
use web\request;

use breadcrumb;
use log;
use album;
use rating;
use user;

use albumNotAccessibleSecurityException;

use organiserActions;

/**
 * Controller for album
 */
class controller extends genericController {
    use organiserActions;

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "new", "insert", "confirm", "delete", "display", "edit", "update", "coverphoto", "unsetcoverphoto"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if ($request->_action=="new" || $request->_action=="insert") {
            $album=new album();
            if (isset($this->request["parent_album_id"])) {
                $album->set("parent_album_id", $this->request["parent_album_id"]);
            }
            $this->setObject($album);
            $this->doAction();
        } else {
            try {
                $album=$this->getAlbumFromRequest();
            } catch (albumNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $album=null;
            }
            if ($album instanceof album) {
                $this->setObject($album);
                $this->doAction();
            } else {
                $this->view = new  static::$viewNotfound($this->request, $this->object);
            }
        }
    }

    /**
     * Get the album based on the query in the request
     * @throws albumNotAccessibleSecurityException
     */
    private function getAlbumFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["album_id"])) {
            $album = new album($this->request["album_id"]);
            if (!$album->lookup()) {
                $album=null;
            }
        } else {
            $album=album::getRoot();
        }
        if ($album && ($user->isAdmin() || $user->getAlbumPermissions($album))) {
            return $album;
        }
        throw new albumNotAccessibleSecurityException(
            "Security Exception: album " .  (int) $this->request["album_id"] .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->canDeletePhotos()) {
            $parentId = $this->object->get("parent_album_id");
            parent::actionConfirm();
            $this->view->setRedirect("album.php?album_id=" . $parentId);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionDelete();
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'insert'
     */
    public function actionInsert() {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

    /**
     * Do action 'new'
     */
    public function actionNew() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

}
