<?php
/**
 * View to update circle
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace circle\view;

use conf\conf;
use circle;
use template\block;
use template\form;
use web\request;

/**
 * Update screen for circle
 */
class update extends view implements \view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        if ($this->request["_action"] == "new") {
            return array(
                translate("return")    => "person.php?_action=people"
            );
        } else {
            if ($this->object->getId() != 0) {
                $returnURL = "circle.php?circle_id=" . $this->object->getId();
            } else {
                $returnURL = "person.php?_action=people";
            }

            return array(
                translate("delete")    => "circle.php?_action=delete&amp;circle_id=" . $this->object->getId(),
                translate("new")       => "circle.php?_action=new",
                translate("return")    => $returnURL
            );
        }
    }


    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $_action = $this->request["_action"];

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $form=new form("form", array(
            "formAction"        => "circle.php",
            "onsubmit"          => null,
            "action"            => $_action == "new" ? "insert" : "update",
            "submit"            => translate("submit")
        ));

        $form->addInputHidden("circle_id", $this->object->getId());

        $form->addInputText("circle_name", $this->object->getName(), translate("Name"),
            sprintf(translate("%s chars max"), 32), 32);

        $form->addTextArea("description", $this->object->get("description"), translate("Description"), 40, 4);

        $members=new block("formFieldsetAddRemove", array(
            "legend"    => translate("members"),
            "objects"   => $this->object->getMembers(),
            "dropdown"  => $this->object->getNewMemberDropdown("_member")
        ));
        $form->addBlock($members);

        $tpl->addBlock($form);

        return $tpl;
    }

    public function getTitle() : string {
        if ($this->request["_action"] == "new") {
            return translate("New circle");
        } else {
            return parent::getTitle();
        }

    }
}
