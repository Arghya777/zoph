<?php
/**
 * View to display circle
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace circle\view;

use conf\conf;
use circle;
use photoNoSelectionException;
use selection;
use template\block;
use web\request;
use user;

/**
 * Display screen for circle
 */
class display extends view implements \view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $actionlinks = array(
            translate("edit")      => "circle.php?_action=edit&amp;circle_id=" . $this->object->getId(),
            translate("delete")    => "circle.php?_action=delete&amp;circle_id=" . $this->object->getId(),
            translate("new")       => "circle.php?_action=new",
        );
        if ($this->object->get("coverphoto")) {
            $actionlinks[translate("unset coverphoto")]=
                "circle.php?_action=update&amp;circle_id=" . $this->object->getId() . "&amp;coverphoto=NULL";
        }
        return $actionlinks;
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $user = user::getCurrent();
        $tpl = new block("display", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
            "obj"               => $this->object,
            "fields"            => $this->object->getDisplayArray(),
            "selection"         => $this->getSelection(),
            "pageTop"           => null,
            "pageBottom"        => null,
            "page"              => null,
            "showMain"          => true
        ));

        if ($user->canSeePeopleDetails()) {
            $tpl->addBlock(new block("definitionlist", array(
                "class" => "display circle",
                "dl"    => $this->object->getDisplayArray()
            )));
        }
        return $tpl;
    }

    private function getSelection() {
        try {
            $selection=new selection($_SESSION, array(
                "coverphoto"    => "circle.php?_action=update&amp;circle_id=" . $this->object->getId() . "&amp;coverphoto=",
                "return"        => "_return=circle.php&amp;_qs=circle_id=" . $this->object->getId()
            ));
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }
        return $selection;
    }
}    
