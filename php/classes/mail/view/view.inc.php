<?php
/**
 * common parts of view mail
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use conf\conf;
use template\block;
use template\form;
use photo;
use user;
use web\request;
use web\view\view as webView;

/**
 * Common parts for mail views
 */
abstract class view extends webView implements \view {
    /** @var web\request holds the request */
    protected $request;

    /** @var pageset holds the photo */
    protected $object;

    /** @var string error message */
    protected $error;

    /** @var string warning message */
    protected $warning;

    /** @var string success message */
    protected $success;

    public function __construct(request $request, photo $photo = null) {
        $this->request=$request;
        $this->object=$photo;
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }
    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("email photo");
    }

    public function setMessage(string $error = null, string $warning = null, string $success = null) : void {
        $this->error = $error ?? null;
        $this->warning = $warning ?? null;
        $this->success = $success ?? null;
    }

    protected function getMailForm(string $action, string $msg) : form {
        $user = user::getCurrent();

        if (isset($this->user) && $this->user instanceof user) {
            $to_name = $this->user->person ? $this->user->person->getName() : "";
            $to_email = $this->user->person ? $this->user->person->getEmail() : "";
        } else {
            $to_name = $to_email = "";
        }

        $from_name = $user->person->getName();
        $from_email = $user->person->getEmail();

        $form = new form("form", array(
            "class"         => "email",
            "formAction"    => "mail.php",
            "action"        => $action,
            "submit"        => translate("email"),
            "onsubmit"      => null
        ));

        $msg = translate("Hi", 0) . " " . $to_name .  ",\n\n" . $msg . "\n\n";
        $msg .= translate("Regards,", 0) . "\n" .  $from_name;

        $form->addInputText("to_name", $to_name, translate("to (name)"), sprintf(translate("%s chars max"), "32"), 32, 24);
        $form->addInputEmail("to_email", $to_email, translate("to (email)"));
        $form->addInputText("from_name", $from_name, translate("from (your name)"), sprintf(translate("%s chars max"), "32"), 32, 24);
        $form->addInputEmail("from_email", $from_email, translate("from (your email)"));
        $form->addTextarea("message", $msg, translate("message"), 70, 5);

        return $form;
    }



}
