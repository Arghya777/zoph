<?php
/**
 * View for display notify user mail compose page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use conf\conf;
use template\block;
use template\form;
use template\template;
use user;
use web\url;

/**
 * This view displays a form to notify a Zoph user of his or her account creation
 */
class notifyuser extends view implements \view {

    /**
     * Output view
     */
    public function view() : block {
        $subject = translate("Your Zoph Account", 0);

        $msg = translate("I have created a Zoph account for you", 0) .  ":\n\n" .  url::get() . "\n";
        $msg .= translate("user name", 0) . ": " .  $this->user->getName() . "\n";

        $form = $this->getMailForm("mailnotify", $msg);

        $form->addInputHidden("user_id", $this->user->getId());
        $form->addInputText("subject", $subject, translate("subject"), sprintf(translate("%s chars max"), "40"), 40);


        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
            "error"             => $this->error,
            "warning"           => $this->warning,
            "success"           => $this->success,
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock($form);

        return $tpl;
    }

    public function setUser(user $user = null) : void {
        $this->user = $user;
    }
}
