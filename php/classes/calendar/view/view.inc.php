<?php
/**
 * common parts of view for calendar
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace calendar\view;

use Time;
use web\request;
use web\view\view as webView;

/**
 * Common parts for calendar views
 */
abstract class view extends webView implements \view {

    public function __construct(protected request $request, protected Time $date) {
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->date->format("F Y");
    }
}
