<?php
/**
 * Controller for calendar
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace calendar;

use conf\conf;
use generic\controller as genericController;
use web\request;
use model as calendar;
use Time;
use user;

/**
 * Controller for calendar
 */
class controller extends genericController {
    protected static $viewDisplay   = view\display::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display"
    );

   /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        $this->doAction();
    }


    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $date = $this->request["date"];
        $year = (int) $this->request["year"];
        $month = (int) $this->request["month"];

        if ($year && $month) {
            $date=new Time($year . "-" . $month . "-01");
        } else if ($date) {
            list($year, $month, $day) = explode("-", $date);
            $date=new Time($year . "-" . $month . "-01");
        } else {
            $date=new Time("first day of this month");
        }


        $this->view=new static::$viewDisplay($this->request, $date);
        $width = ((THUMB_SIZE + 40 + 10 + 10) * 7) + 60;
        $this->view->setStyle("body { width: " . $width . "px; }");
        
    }

}
