<?php
/**
 * Controller for pagesets
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace pageset;

use conf\conf;
use generic\controller as genericController;
use web\request;
use page;
use pageset;
use user;

/**
 * Controller for pageset
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewPagesets  = view\pagesets::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "addpage", "confirm", "delete", "display", "edit", "insert",
        "movedown", "moveup", "new", "pagesets", "remove", "update"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);

        $pagesetId = $request["pageset_id"] ?? null;

        $this->object = new pageset($pagesetId);
        $this->object->lookup();

        $this->doAction();
    }

    /**
     * Do action 'addpage'
     */
    protected function actionAddpage() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $page = new page($this->request["page_id"]);
            $this->object->addPage($page);
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'confirm'
     */
    protected function actionConfirm() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionConfirm();
            $this->view->setRedirect("pageset.php?_action=pagesets");
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    protected function actionDelete() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'remove'
     */
    protected function actionRemove() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $page = new page($this->request["page_id"]);
            $this->object->removePage($page);
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'edit'
     */
    protected function actionEdit() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'moveup'
     */
    protected function actionMoveup() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $page = new page($this->request["page_id"]);
            $this->object->moveUp($page);
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'movedown'
     */
    protected function actionMovedown() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $page = new page($this->request["page_id"]);
            $this->object->moveDown($page);
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'pagesets'
     */
    protected function actionPagesets() {
        $this->view = new static::$viewPagesets($this->request);
    }


    /**
     * Do action 'update'
     */
    protected function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin()) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'insert'
     */
    protected function actionInsert() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $pageset = new pageset();
            $pageset->set("user", user::getCurrent()->getId());
            $this->setObject($pageset);
            parent::actionInsert();
        }
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $this->view->setRedirect("pageset.php?pageset_id=" . $this->object->getId());
    }
}
