<?php
/**
 * View for display pageset
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace pageset\view;

use page;
use pageset;
use template\block;
use template\template;
use user;

/**
 * This view displays pageset
 */
class display extends view implements \view {

    /**
     * Output view
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(new block("definitionlist", array(
            "class" => "display pageset",
            "dl"    => $this->object->getDisplayArray()
        )));

        $tpl->addBlock(new block("pagesetPages", array(
            "pages"     => page::getTable($this->object->getPages(), $this->object),
            "pagesetId" => $this->object->getId(),
            "dropdown"  => template::createDropdown("page_id", 0,
                template::createSelectArray(page::getRecords("title"), array("title"), true), true)
        )));

        return $tpl;
    }
}
