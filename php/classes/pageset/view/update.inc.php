<?php
/**
 * View for edit pageset
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace pageset\view;

use conf\conf;
use template\form;
use template\block;
use template\template;
use user;
use web\request;

/**
 * This view displays the pageset page when editing
 */
class update extends view implements \view {
    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        if ($this->object->getId() != 0) {
            $actionlinks=array(
                translate("return") => "pageset.php?_action=display&amp;pageset_id=" . $this->object->getId(),
                translate("delete") => "pageset.php?_action=delete&amp;pageset_id=" . $this->object->getId()
            );
        } else {
            $actionlinks=array(
                translate("return") =>  "pageset.php?_action=pagesets"
            );
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
        }

        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $form = new form("form", array(
            "formAction"    => "pageset.php",
            "class"         => "pageset",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate($action, 0)
        ));


        $origPosDropdown = template::createDropdown("orig_pos", $this->object->get("orig_pos"), array(
            "top"       => translate("Top",0),
            "bottom"    => translate("Bottom",0))
        );

        $form->addInputHidden("pageset_id", $this->object->getId());
        $form->addInputText("title", $this->object->get("title"), translate("title"));
        $form->addDropdown("show_orig", $this->object->getOriginalDropdown(), translate("show original page"));
        $form->addDropdown("orig_pos", $origPosDropdown, translate("position of original"));

        $tpl->addBlock($form);

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        if ($this->object->getId()==0) {
            return translate("Add pageset");
        } else if (trim($this->object->get("title"))=="") {
            return translate("pageset");
        } else {
            return $this->object->get("title");
        }
    }

}
