<?php
/**
 * XMP Data - Interprete and return XMP data
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace xmp;

use ArrayAccess;
use Iterator;
use Countable;

/**
 * @author Jeroen Roos
 * @package Zoph
 */
class data implements ArrayAccess, Iterator, Countable  {

    private $xmpdata = array();

    /**
     * Create new xmp/data object
     * @param array data to inject
     */
    public function __construct(array $data=null) {
        $this->xmpdata = $data;
    }

    public function getRDFDescription(mixed $off) : mixed {
        return $this["x:xmpmeta"]["rdf:RDF"]["rdf:Description"][$off];
    }

    /**
     * For ArrayAccess: does the offset exist
     * @param mixed offset
     * @return bool offset exists
     */
    public function offsetExists(mixed $off) : bool {
        return (isset($this->xmpdata[$off]));
    }
    /**
     * For ArrayAccess: Get value of parameter
     * @param mixed offset
     * @return mixed value
     */
    public function offsetGet(mixed $off) : mixed {
        $data = $this->xmpdata[$off] ?? null;
        if (is_array($data)) {
            if (sizeof($data) === 0) {
                return null;
            } else {
                return new self($data);
            }
        } else {
            return $data;
        }
    }

    /**
     * For ArrayAccess: Set value of parameter
     * @param mixed offset
     * @param mixed value
     */
    public function offsetSet(mixed $off, mixed $val) : void {
        $this->xmpdata[$off] = $val;
    }

    /**
     * For ArrayAccess: Unset value of parameter
     * @param mixed offset
     */
    public function offsetUnset(mixed $off) : void {
        if (isset($this->xmpdata[$off])) {
            unset($this->xmpdata[$off]);
        }
    }

    /**
     * For ObjectAccess: Get value of parameter
     * @param mixed offset
     * @return mixed value
     */
    public function __get(mixed $off) : mixed {
        return $this->offsetGet($off);
    }

    /**
     * For Iterator access - rewind to begining
     */
    public function rewind() : void {
        reset($this->xmpdata);
    }

    /**
     * For Iterator access - get current
     * @return mixed current entry
     */
    public function current() : mixed {
        return current($this->xmpdata);
    }

    /**
     * For Iterator access - get key of current entry
     * @return mixed current key
     */
    public function key() : mixed {
        return key($this->xmpdata);
    }

    /**
     * For Iterator access - move to next entry
     */
    public function next() : void {
        next($this->xmpdata);
    }

    /**
     * For Iterator access - is the current entry valid
     * @return bool valid
     */
    public function valid() : bool {
        return key($this->xmpdata) !== null;
    }

    /**
     * For Countable access - get size of data
     */
    public function count() : int {
        return count($this->xmpdata);
    }
}
?>
