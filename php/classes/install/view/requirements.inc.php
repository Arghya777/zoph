<?php
/**
 * View for display requirements check
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use conf\conf;
use template\block;
use web\request;
use requirements\check as checkRequirements;

/**
 * Check requirements
 */
class requirements extends view {

    /** @var requirements */
    private $requirements;

    /**
     * View for install
     * @return template\template
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $text = $this->requirements->show();
        if ($this->requirements->getStatus()) {
            $text->addBlock(new block("button", array(
                "button"        => "next",
                "button_class"  => "install",
                "button_url"    => "install.php?_action=iniform"
            )));
        } else {
            $text->addBlock(new block("message", array(
                "class"         => "error",
                "title"         => "unmet requirements",
                "text"       => "Not all requirements have been met. Zoph installation cannot continue. " .
                    "Please fix the issues above and retry."
            )));
            $text->addBlock(new block("button", array(
                "button"        => "retry",
                "button_class"  => "install",
                "button_url"    => "install.php?_action=requirements"
            )));
        }
        $tpl->addBlock($text);

        return $tpl;
    }

    public function addRequirements(checkRequirements $requirements) {
        $this->requirements = $requirements;
    }

}
