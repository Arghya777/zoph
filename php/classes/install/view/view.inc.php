<?php
/**
 * Common parts for install view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use template\block;
use template\page;
use template\template;
use web\request;
use web\view\view as webView;

/**
 * This is a base view for the install pages, that contains common parts for all views
 */
abstract class view extends webView implements \view {
    /** * @var string title */
    protected $title;

    /** * @var array request variables */
    protected $vars;

    /** * @var array template\result results */
    protected $results = array();

    /** *@var array javascripts to add */
    protected $javascripts = array();

    /** *@var array javascripts to include */
    protected $scripts = array();

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request) {
        $this->vars=$request->getRequestVars();
    }

    protected function getIcons() : block {
        $current = substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);

        $icons = array(
            "display"       => template::getImage("icons/install.png"),
            "requirements"  => template::getImage("icons/requirements.png"),
            "iniform"       => template::getImage("icons/textedit.png"),
            "inifile"       => template::getImage("icons/inifile.png"),
            "dbcheck"       => template::getImage("icons/check.png"),
            "dbpass"        => template::getImage("icons/password.png"),
            "database"      => template::getImage("icons/database.png"),
            "tables"        => template::getImage("icons/table.png"),
            "adminuser"     => template::getImage("icons/user.png"),
            "config"        => template::getImage("icons/configuration.png"),
            "finish"        => template::getImage("icons/diapositive.png")
        );

        return new block("install", array(
            "icons"     => $icons,
            "selected"  => $current
        ));
    }

    public function display($template = "install") : void {
        $page = new page("install", array(
            "lang"          => "en",
            "title"         => $this->getTitle(),
            "version"       => VERSION,
            "scripts"       => $this->scripts,
            "javascript"    => $this->javascripts
        ));
        $page->addBlock($this->view());

        echo $page;
    }


    /**
     * Get title
     * @return string title
     */
    public function getTitle() : string {
        return $this->title ?: translate("Zoph Installation");
    }

    /**
     * Set results
     * @param array array of template\result
     */
    public function setResults(array $results) {
        $this->results = $results;
    }

}


