<?php
/**
 * View to show results of database creation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use conf\conf;
use template\block;
use web\request;

/**
 * Check database
 */
class dbcheck extends view {

    /** @var string error to display instead of form */
    private $error;
    private $dbinfo;

    /**
     * View for install
     * @return template\block
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        if (isset($this->error)) {
            $tpl->addBlock(new block("message", array(
                "class" => "error",
                "title" => "Problem with INI file",
                "text"  => $this->error
            )));

            $tpl->addBlock(new block("button", array(
                "button"        => "retry",
                "button_class"  => "install",
                "button_url"    => "install.php?_action=dbcheck"
            )));
        } else {
           $tpl->addBlock(new block("text", array(
                "title"     => translate("INI file loaded", false),
                "text"      => array("The ini file was succesfully loaded. You can continue the install process."),
                "class"     => "install"
           )));

           if (isset($this->dbinfo)) {
                $tpl->addBlock(new block("message", array(
                    "class" => "warning",
                    "title" => "Database already exists",
                    "text"  => $this->dbinfo
                )));

                $tpl->addBlock(new block("button", array(
                    "button"        => "finish installation",
                    "button_class"  => "install",
                    "button_url"    => "install.php?_action=finish"
                )));
           }


            $tpl->addBlock(new block("button", array(
                "button"        => "next",
                "button_class"  => "install",
                "button_url"    => "install.php?_action=dbpass"
            )));

        }

        return $tpl;
    }

    public function setError(string $error) {
        $this->error = $error;
    }

    public function setDatabaseInfo(array $dbinfo) {
        $this->dbinfo = new block("zophinfo", array(
            "infoArray" => $dbinfo,
            "caption"   => "In database"
        ));
    }

}
