<?php
/**
 * View to show results of database creation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use conf\conf;
use template\block;
use web\request;

/**
 * Display database creation
 */
class database extends view {

    /**
     * View for install
     * @return template\block
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $results = array_merge(
            array("The database has been created and access rights have been set. Click next to create tables."),
            $this->results
        );

        $text = new block("text", array(
            "title"     => translate("Database created", false),
            "text"      => $results,
            "class"     => "install"
        ));

        $text->addBlock(new block("button", array(
            "button"        => "next",
            "button_class"  => "install",
            "button_url"    => "install.php?_action=tables"
        )));

        $tpl->addBlock($text);

        return $tpl;
    }

}
