<?php
/**
 * View for setting the database root user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use conf\conf;
use template\block;
use template\form;
use web\request;

/**
 * Set database root user to create db and set privileges
 */
class dbpass extends view {

    /**
     * View for install
     * @return template\block
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $form=new form("form", array(
            "class"         => "dbuser",
            "formAction"    => "install.php",
            "action"        => "database",
            "submit"        => "next",
            "onsubmit"      => null,
            "submit_class"  => "big green"
        ));

        $form->addInputText("user", "root", translate("Username"),
            "User that has administrative privileges on the database. Usually 'root'.");
        $form->addInputPassword("admin_pass", translate("Admin Password"));
        $tpl->addBlock($form);

        return $tpl;
    }
}
