<?php
/**
 * common parts of view for pageset
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace admin\view;

use conf\conf;
use template\block;
use template\template;
use user;
use web\request;
use web\view\view as webView;

/**
 * Common parts for page views
 */
abstract class view extends webView implements \view {

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Adminpage");
    }
}
