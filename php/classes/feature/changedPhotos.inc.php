<?php
/**
 * Feature class for changed photos
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */
namespace feature;

use db\clause;
use db\param;
use db\select;
use photo\collection;
use template\block;
use user;

use DateInterval;
use PDO;
/**
 * Feature class
 * Base class for changed photos
 * currently used on the welcome page of Zoph
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class changedPhotos extends base {
    protected $type = "changed";
    protected $title = "Recently changed photos";
    protected $width = THUMB_SIZE + 10;

    public function getData(int $count = 10) {
        $photos = collection::createFromVars(array(
            "_order"        => "timestamp",
            "_dir"          => "desc"
        ))->subset(0,$count)->toArray();

        return parent::getPhotoData($photos);
    }

    public function getMore() : ?block {
        $user = user::getCurrent();
        $intervals = $this->getIntervals();
        foreach ($intervals as $phpInterval => $sqlInterval) {
            $qry = (new select(array("p" => "photos")));
            $qry->addFunction(array("count" => "count(*)"));
            $where = new clause("timestamp > date_sub(now(), interval " . $sqlInterval . ")");
            if (!$user->canSeeAllPhotos()) {
                $qry->join(array("vpu" => "view_photo_user"), "p.photo_id = vpu.photo_id");
                $where->addAnd(new clause("vpu.user_id = :userid"));
                $qry->addParam(new param("userid", $user->getId(), PDO::PARAM_INT));
            }
            $qry->where($where);


            $count = $qry->getCount();
            if ($count > 10) {
                break;
            }
        }

        if ($count > 0) {
            $data = $this->getFormattedDates(new DateInterval($phpInterval));
            return new block("link", array(
                "href"  => "photos.php?_timestamp_op=%3E%3D&amp;timestamp=" . $data[1],
                "link"  => sprintf(translate("See %s photos modified in the past %s...", false), $count, $data[0])
            ));
        }
        return null;
    }
}
