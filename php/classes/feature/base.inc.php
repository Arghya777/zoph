<?php
/**
 * Feature class
 * Base class for featured photos
 * currently used on the welcome page of Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */
namespace feature;

use DateInterval;
use DateTimeImmutable;

/**
 * Feature class
 * Base class for featured photos
 * currently used on the welcome page of Zoph
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class base {
    protected function getPhotoData(array $photos) : array {
        $features=array_values(array_map(function($obj) { return $obj->getId(); }, $photos));
        return array(
            "type"      => $this->getType(),
            "objects"   => $features
        );
    }

    protected function getOrganizerData(array $organizers) : array {
        $features = array();
        foreach ($organizers as $organizer) {
            $features[] = array(
                "id"    => $organizer->getId(),
                "title" => $organizer->getName(),
                "cover" => $organizer->getAutoCover()?->getId() ?? 0,
            );
        }
        return array(
            "type"      => $this->getType(),
            "objects"   => $features
        );
    }

    public function getType() : string {
        return $this->type;
    }

    public function getTitle() : string {
        return $this->title;
    }

    public function getWidth() : int {
        return $this->width;
    }

    protected function getIntervals() {
        return array(
            "P1D"   => "1 day",
            "P7D"   => "7 day",
            "P2W"   => "14 day",
            "P1M"   => "1 month",
            "P2M"   => "2 month",
            "P3M"   => "3 month",
            "P6M"   => "6 month",
            "P1Y"   => "1 year",
            "P2Y"   => "2 year",
            "P3Y"   => "3 year",
            "P4Y"   => "4 year",
            "P5Y"   => "5 year"
        );
    }

    public function getFormattedDates(DateInterval $interval) {
        $now = new DateTimeImmutable();

        $then = $now->sub($interval);

        $diff = date_diff($then, $now);
        $days = $diff->format("%a");

        if ($days < 27) {
            $timeDisplay = $interval->format("%d days");
        } else if ($days < 364) {
            $timeDisplay = $interval->format("%m months");
        } else {
            $timeDisplay = $interval->format("%y years");
        }


        return array($timeDisplay, $then->format("Y-m-d"));
    }

    public static function getAll() : array {
        return array(
            "random"    => randomPhotos::class,
            "recent"    => recentPhotos::class,
            "changed"   => changedPhotos::class,
            "albums"    => newestAlbums::class
        );
    }
}
