<?php
/**
 * Feature class for newest albums
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */
namespace feature;

use album;
use db\db;
use db\clause;
use db\param;
use db\select;
use PDO;
use photo\collection;
use template\block;
use user;

/**
 * Feature class
 * Base class for featured photos
 * currently used on the welcome page of Zoph
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class newestAlbums extends base {
    protected $type = "albums";
    protected $title = "Newest albums";
    protected $width = THUMB_SIZE + 40 + 10;

    public function getData(int $count = 10) {
        $user=user::getCurrent();

        $qry = (new select(array("vad" => "view_albums_details")))->addOrder("newest DESC")->addLimit($count);

        if (!$user->canSeeAllPhotos()) {
            $qry->join(array("gp" => "group_permissions"), "vad.album_id=gp.album_id")
                ->join(array("gu" => "groups_users"), "gp.group_id=gu.group_id");
            $qry->where(new clause("gu.user_id=:userid"));
            $qry->addParam(new param(":userid", (int) $user->getId(), PDO::PARAM_INT));
        }

        $result=db::query($qry);
        $albums = array();
        if ($result) {
            foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $albumArray) {
                $album = new album($albumArray["album_id"]);
                $album->lookup();
                $albums[] = $album;
            }
        }

        return parent::getOrganizerData($albums);
    }

    public function getMore() : ?block {
        $albumCount=album::getCount();
        $albumPhotoCount = album::getRoot()->getTotalPhotoCount();

        if ($albumPhotoCount > 0) {
            return new block("link", array(
                "href"  => "album.php",
                "link"  => sprintf(translate("See %s albums, containing %s photos", false), $albumCount, $albumPhotoCount)
            ));
        } else {
            return null;
        }
    }
}
