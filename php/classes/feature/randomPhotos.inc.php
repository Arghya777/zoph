<?php
/**
 * Feature class for random photos
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */
namespace feature;

use photo\collection;
use template\block;
use user;

/**
 * Feature class
 * Class for random photos
 * currently used on the welcome page of Zoph
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class randomPhotos extends base {
    protected $type = "random";
    protected $title = "Random photos";
    protected $width = THUMB_SIZE + 10;

    public function getData(int $count = 10) {
        $photos=collection::createFromVars(array(
            "rating"        => user::getCurrent()->prefs->get("random_photo_min_rating"),
            "_rating_op"    => ">="
        ))->random($count)->toArray();
        return parent::getPhotoData($photos);
    }

    public function getMore() : ?block {
        $numPhotos=count(collection::createFromVars(array()));
        if ($numPhotos > 0) {
            return new block("link", array(
                "href"  => "photos.php",
                "link"  => sprintf(translate("See all %s photos...", false), $numPhotos)
            ));
        } else {
            return null;
        }
    }

}
