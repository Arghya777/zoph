<?php
/**
 * View to show migration results
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use conf\conf;
use upgrade\migrations;
use template\block;
use template\template;
use web\request;

/**
 * Display executed migrations
 */
class upgrade extends view implements \view {
    /** @var array completed migrations */
    private $completeMigrations = array();

    public function setComplete(array $complete) : void {
        $this->completeMigrations=$complete;
    }

    /**
     * Retuirn the view
     * @return template\block upgrade view
     */
    public function view() : block {
        return new block("upgrade", array(
            "title"         => conf::get("interface.title"),
            "version"       => VERSION,
            "migrations"    => $this->completeMigrations,
            "button"       => translate("finish"),
            "button_url"   => "zoph.php"
        ));
    }
}
