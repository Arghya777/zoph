<?php
/**
 * View for display pending migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade\view;

use conf\conf;
use upgrade\migrations;
use template\block;
use template\form;
use template\result;
use template\template;

/**
 * Display pending migrations
 */
class display extends view implements \view {

    /**
     * View for displaying pending migrations
     * @return template\template
     */
    public function view() : block {
        $showMigrations = array();
        foreach ($this->migrations->get() as $migration) {
            $steps = array();
            foreach ($migration->show() as $step) {
                $result = new result(result::TODO, $step);
                $result->setIcon(template::getImage("icons/migration.png"));
                $steps[] = $result;
            }
            $showMigrations[] = new block("migration", array(
                "desc" => $migration->getDesc(),
                "results" => $steps
            ));
        }

        $message=new block("message", array(
            "title"     => translate("backup"),
            "class"     => "warning",
            "text"      => translate("it is highly recommended that you make a backup of your database prior to performing an upgrade")
        ));
        $message->addActionlinks(array(
            translate("make backup")   => "backup.php?_return=upgrade.php"
        ));

        $form=new form("form", array(
            "class"         => "dbuser",
            "formAction"    => "upgrade.php",
            "action"        => "upgrade",
            "submit"        => translate("start upgrade"),
            "onsubmit"      => null,
            "submit_class"  => "big green"
        ));

        $form->addInputText("user", "", translate("Username"),
            "Perform the upgrade with a different database user. Use this if you have created a database user with limited privileges. Leave empty to use default.");
        $form->addInputPassword("pass", translate("Password"));

        $tpl=new block("upgrade", array(
            "title"         => conf::get("interface.title"),
            "version"       => VERSION,
            "migrations"    => $showMigrations,
        ));
        $tpl->addBlocks(array($message, $form));
        return $tpl;
    }
}
