<?php
/**
 * View to show the about page of Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph\view;

use report;
use template\block;
use template\page;
use template\template;
use user;
use web\view\view;
use zoph\zoph;

/**
 * Display info screen
 */
class info extends view implements \view {

    /**
     * Get view
     * @return template view
     */
    public function view() : block {

        if (user::getCurrent()->isAdmin()) {
            $zophinfo = new block("zophinfo", array(
                "infoArray" => report::getInfoArray(),
                "caption"   => translate("About")
            ));
        }


        return new block("info", array(
            "zophinfo"  => $zophinfo ?? null,
            "title"     => $this->getTitle(),
            "mailaddr"  => template::getImage("mailaddr.png"),
            "credits"   => zoph::getCredits()
        ));
    }

}
