<?php
/**
 * View to show the login page of Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph\view;

use conf\conf;
use language;
use template\block;
use template\page;
use user;
use web\request;
use web\view\view;
use upgrade\migrations;


/**
 * Display login screen
 */
class login extends view implements \view {

    /**
     * Get view
     * @return template view
     */
    public function view() : ?block {
        return null;
    }

   /**
    * Display logon screen
    * @codeCoverageIgnore
    */
    public function display($template = "logon") : void {
        $user = new user();
        $user->loadLanguage();

        echo new page($template, array(
            "lang"      => language::getCurrentISO(),
            "title"     => conf::get("interface.title"),
            "redirect"  => urlencode($this->request["redirect"] ?? ""),
            "error"     => $this->getLoginError(),
            "warning"   => $this->checkMigrations(),
            "menu"      => ""
        ));

    }

    private function checkMigrations() : ?string {
        $migrations = new migrations();
        if ($migrations->check(conf::get("zoph.version"))) {
            return translate("The upgrade of Zoph has not been completed, only admin user can logon");
        }
        return null;
    }

    private function getLoginError() : string {
        return match($this->request["error"] ?? "") {
            "PWDFAIL"   => translate("You have entered an incorrect username/password combination"),
            "ADMINONLY" => translate("Only admin users can logon now"),
            default     => ""
        };
    }

}
