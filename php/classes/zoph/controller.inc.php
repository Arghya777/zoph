<?php
/**
 * Controller for the welcome screen
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph;

use generic\controller as genericController;
use web\request;

/**
 * Controller for the welcome and info screen
 */
class controller extends genericController {
    protected static $viewDisplay   = view\display::class;
    protected static $viewInfo      = view\info::class;
    protected static $viewReports   = view\reports::class;
    protected static $viewLogin     = view\login::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "info", "reports", "login"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);

        if (defined("LOGON")) {
            $this->actionLogin();
        } else {
            $this->doAction();
        }
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $this->view = new static::$viewDisplay($this->request);
    }

    /**
     * Do action 'login'
     */
    public function actionLogin() {
        $this->view = new static::$viewLogin($this->request);
    }

    /**
     * Do action 'info'
     */
    public function actionInfo() {
        $this->view = new static::$viewInfo($this->request);
    }

    /**
     * Do action 'report'
     */
    public function actionReports() {
        $this->view = new static::$viewReports($this->request);
    }

}
