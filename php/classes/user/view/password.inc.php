<?php
/**
 * View to update user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use album;
use conf\conf;
use person;
use user;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Update user password
 */
class password extends view {
    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        return array(
            translate("return")    => "user.php?user_id=" . $this->object->getId()
        );
    }

    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        if (isset($this->msg)) {
            $tpl->addblock($this->msg->view());
        }

        $form=new form("form", array(
            "class"         => "password",
            "formAction"    => "user.php",
            "action"        => "update",
            "submit"        => "change",
            "onsubmit"      => null
        ));

        $form->addInputHidden("user_id", $this->object->getId());
        $form->addInputPassword("_password", translate("Password"));
        $form->addInputPassword("_confirm", translate("Confirm"));
        $form->addBlock(new block("message", array(
            "id"            => "pwdmatcherror",
            "class"         => "error",
            "title"         => "Passwords do not match",
            "text"          => "Make sure password and confirm match"
        )));


        $script = new block("script", array(
            "script"        => array("window.addEventListener(\"load\", function(){ zPass.init('_password', '_confirm', 'pwdmatcherror') })" )
        ));

        $tpl->addBlock($form);
        $tpl->addBlock($script);
        return $tpl;

    }

    public function getTitle() : string {
        return sprintf(translate("Change password for %s"), $this->object->getName());
    }


}
