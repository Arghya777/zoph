<?php
/**
 * View to display users
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use album;
use conf\conf;
use user;
use template\block;
use web\request;

/**
 * Display screen for users
 */
class users extends view {

    private $objects = array();

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        return array(
            translate("new")       => "user.php?_action=new",
        );
    }

    public function setObjects(array $objects) : void {
        $this->objects = $objects;
    }

    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $users = array();
        foreach ($this->objects as $user) {
            $displayUser = new \stdClass();

            $displayUser->name      = $user->getName();
            $displayUser->url       = $user->getURL();
            $displayUser->lastlogin = $user->get("lastlogin");
            $displayUser->person    = null;

            $user->lookupPerson();
            if ($user->person) {
                $displayUser->person    = $user->person->getName();
                $displayUser->personURL = $user->person->getURL();
            }

            $displayUser->actionlinks = array(
                translate("display")        => "user.php?user_id=" . $user->getId()
            );

            if (count(album::getNewer($user, $user->getLastNotify())) > 0) {
                $displayUser->actionlinks[translate("Notify User", 0)] =
                    "mail.php?_action=notify&amp;user_id=" .
                    $user->getId() . "&amp;shownewalbums=1";
            }
            $users[] = $displayUser;
        }

        return new block("displayUsers", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "users"         => $users,
        ));
    }

    public function getTitle() : string {
        return translate("Users", false);
    }
}
