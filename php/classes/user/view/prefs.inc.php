<?php
/**
 * View to display user prefs
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use conf\conf;
use language;
use template\block;
use template\form;
use template\template;
use user\prefs as userPrefs;
use web\request;

/**
 * Display screen for user prefs
 */
class prefs extends view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : ?array {
        return array(
           translate("change password") => "user.php?_action=password"
        );
    }


    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $user = $this->object;
        $langs = language::getAll();
        $languages=array();
        $languages[null] = translate("Browser Default");
        foreach ($langs as $language) {
            $languages[$language->iso] = $language->name;
        }
        
        $tpl = new block("prefs", array(
            "title"             => $this->getTitle(),
            "prefs"             => $user->prefs,
            "userId"            => $user->getId(),
            "userName"          => $user->getName(),
            "isAdmin"           => $user->isAdmin(),
            "languages"         => $languages,
            "sortorder"         => userPrefs::getSortorder(),
            "defaultWarning"    => "",
            "autocomplete"      => conf::get("interface.autocomplete")
        ));
        $tpl->addActionlinks($this->getActionlinks());
        return $tpl;
    }

    public function getTitle() : string {
        return match($this->object->getId()) {
            -1      => translate("Default preferences"),
            default => translate("Preferences")
        };
    }
}
