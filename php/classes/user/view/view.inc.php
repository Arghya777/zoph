<?php
/**
 * Base class for user views
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use template\message;
use template\template;
use web\request;
use web\view\view as webView;
use user;

/**
 * Holds the common parts for the user views
 */
abstract class view extends webView implements \view {
    /** @var template\message message to display on */
    protected $msg = null;

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request, protected ?user $object = null) {
    }

    /**
     * Get title
     * @return string title
     */
    public function getTitle() : string {
        return $this->object->getName();
    }

    /**
     * Set message to be displayed
     * @param template\message message
     */
    public function setMessage(?message $msg) : void {
        $this->msg = $msg;
    }

}
