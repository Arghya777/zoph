<?php
/**
 * View for the import page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace import\view;

use conf\conf;
use template\block;
use template\template;
use web\request;
use web\view\view;

/**
 * This view displays the import page
 */
class display extends view implements \view {

    /** * @var array request variables */
    protected $vars;

    protected $title = "import";

    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request) {
        $this->vars=$request->getRequestVars();
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }

    /**
     * Output view
     */
    public function view() : block {
        $javascript=
            "translate=new Array();\n" .
            "translate['retry']='" .trim(translate("retry", false)) . "';\n" .
            "translate['delete']='" .trim(translate("delete", false)) . "';\n" .
            "translate['import']='" .trim(translate("import", false)) . "';\n" .
            "parallel=" . (int) conf::get("import.parallel")  . ";\n";

        if (conf::get("import.upload")) {
            $upload=new block("uploadform", array(
                "action"    => "import.php?_action=upload",
            ));
        } else {
            $upload=translate("Uploading photos has been disabled in configuration.");
        }

        return new block("import", array(
            "title"     => $this->getTitle(),
            "upload"    => $upload,
            "javascript" => $javascript,
        ));
    }

    /**
     * Get title
     */
    public function getTitle() : string {
        return translate("Import");
    }

}
