<?php
/**
 * Controller for person
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person;

use conf\conf;
use circle;
use generic\controller as genericController;
use log;
use web\request;
use person;
use user;

use personNotAccessibleSecurityException;
use circleNotAccessibleSecurityException;

use organiserActions;

/**
 * Controller for person
 */
class controller extends genericController {
    use organiserActions;

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotFound  = view\notfound::class;
    protected static $viewPeople    = view\people::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert",
        "new", "people", "update", "coverphoto", "unsetcoverphoto"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if ($request->_action=="new") {
            $this->setObject(new person());
            $this->doAction();
        } else if ($request->_action=="people") {
            $this->view = new static::$viewPeople($this->request, $this->getCircleFromRequest());
        } else {
            try {
                $person=$this->getPersonFromRequest();
            } catch (personNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $person=null;
            }

            if ($person instanceof person) {
                $this->setObject($person);
                $this->doAction();
            } else {
                $this->view = new static::$viewNotFound($this->request);
            }
        }
    }

    /**
     * Get the person based on the query in the request
     * @throws personNotAccessibleSecurityException
     */
    private function getPersonFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["name"])) {
            $people = person::getByName($this->request["name"]);
            if ($people && count($people) == 1) {
                $person = array_shift($people);
            }
        } else if (isset($this->request["person_id"])) {
            $person = new person($this->request["person_id"]);
            $person->lookup();
        } else {
            $person = new person();
        }
        if ($user->isAdmin() || $person->isVisible()) {
            return $person;
        }
        throw new personNotAccessibleSecurityException(
            "Security Exception: person " . $person->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Get the circle based on the query in the request
     * @throws personNotAccessibleSecurityException
     */
    private function getCircleFromRequest() : ?circle {
        $user=user::getCurrent();

        $circle_id = $this->request["circle_id"];

        if (empty($circle_id)) {
            return null;
        }
        $circle=new circle($this->request["circle_id"]);
        $circle->lookup();
        if ($circle->isVisible() && (!$circle->isHidden() && $user->canSeeHiddenCircles())) {
            return $circle;
        }

        throw new circleNotAccessibleSecurityException(
            "Security Exception: circle " . $circle->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            parent::actionConfirm();
            $this->view->setRedirect("person.php?_action=people");
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($user->canEditOrganizers()) {
            if (isset($this->request["_circle"]) && ((int) $this->request["_circle"] > 0)) {
                $circle = new circle((int) $this->request["_circle"]);
                $circle->addMember($this->object);
            }

            if (is_array($this->request["_remove"])) {
                foreach ($this->request["_remove"] as $circleId) {
                    $circle = new circle((int) $circleId);
                    $circle->removeMember($this->object);
                }
            }
            parent::actionUpdate();
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

}
