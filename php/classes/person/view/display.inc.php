<?php
/**
 * View for display photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use conf\conf;
use template\block;
use web\request;

use person;
use selection;
use user;

use photoNoSelectionException;
use pageException;

/**
 * This view displays the "person" page
 */
class display extends view {

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();

        if ($user->canEditOrganizers()) {
            $actionlinks=array(
                translate("edit")   => "person.php?_action=edit&amp;person_id=" . $this->object->getId(),
                translate("delete") => "person.php?_action=delete&amp;person_id=" . $this->object->getId(),
                translate("new")    => "person.php?_action=new"
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks[translate("unset coverphoto")]="person.php?_action=unsetcoverphoto&amp;person_id=" . $this->object->getId();
            }
        }
        return $actionlinks;

    }

    /**
     * Output view
     */
    public function view() : block {
        $user=user::getCurrent();
        $photosOf = $this->object->getPhotoCount();
        $photosBy = $this->object->getPhotographer()->getPhotoCount();


        $selection=$this->getSelection();


        try {
            $pageset=$this->object->getPageset();
            $page=$this->object->getPage($this->request, $this->request["pagenum"]);
            $showOrig=$this->object->showOrig($this->request["pagenum"]);
        } catch (pageException $e) {
            $showOrig=true;
            $page=null;
        }

        $mainActionlinks=array();

        if ($photosOf > 0) {
            $mainActionlinks[$photosOf . " " . translate("photos of")]  = "photos.php?person_id=" . $this->object->getId();
        }

        if ($photosBy > 0) {
            $mainActionlinks[$photosBy . " " . translate("photos by")]  = "photos.php?photographer_id=" . $this->object->getId();
        }

        $tpl=new block("display", array(
            "title"             => $this->getTitle(),
            "obj"               => $this->object,
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => $mainActionlinks,
            "selection"         => $selection,
            "page"              => $page,
            "pageTop"           => $this->object->showPageOnTop(),
            "pageBottom"        => $this->object->showPageOnBottom(),
            "showMain"          => $showOrig
        ));

        if ($user->canSeePeopleDetails()) {
            $dl=$this->object->getDisplayArray();

            $dl = array_merge($dl, $this->getLocationLinks());
        }
        if ($this->object->get("notes")) {
            $dl[translate("notes")]=$this->object->get("notes");
        }

        $dl = array_merge($dl, $this->getCircles());
        $tpl->addBlock(new block("definitionlist", array(
            "dl"    => $dl,
            "class" => ""
        )));
        return $tpl;
    }

    private function getSelection() {
        $selection=null;
        if (user::getCurrent()->canEditOrganizers()) {
            try {
                $selection=new selection($_SESSION, array(
                    "coverphoto"    => "person.php?_action=coverphoto&amp;person_id=" . $this->object->getId() . "&amp;coverphoto=",
                    "return"        => "_return=person.php&amp;_qs=person_id=" . $this->object->getId()
                ));
            } catch (photoNoSelectionException $e) {
                $selection=null;
            }
        }

        return $selection;
    }

    private function getCircles() {
        $circles=$this->object->getCircles();
        $circleLinks=array();
        if ($circles) {
            foreach ($circles as $circle) {
                $circle->lookup();
                $circleLinks[]= new block("link", array(
                                    "href"      => $circle->getURL(),
                                    "target"    => "",
                                    "link"      => $circle->getName()
                                ));
            }
        }
        if (!empty($circleLinks)) {
            return array(
                translate("circles") => implode(", ", $circleLinks)
            );
        } else {
            return array();
        }
    }

    private function getLocationLinks() {
        $dl = array();
        /**
         * @todo All the link blocks here could be generated by the objects themselves
         *       saving a huge amount of more-or-less duplicate code
         */
        if ($this->object->getEmail()) {
            $mail=new block("link", array(
                "href"      => "mailto:" . e($this->object->getEmail()),
                "target"    =>  "",
                "link"      => e($this->object->getEmail())
            ));
            $dl[translate("email")]=$mail;
        }

        if ($this->object->home) {
            $home=new block("link", array(
                "href"      => "place.php?place_id=" . $this->object->get("home_id"),
                "target"    => "",
                "link"      => $this->object->home->get("title")
            ));
            $dl[translate("home location")]=$home;
        }

        if ($this->object->work) {
            $home=new block("link", array(
                "href"      => "place.php?place_id=" . $this->object->get("work_id"),
                "target"    => "",
                "link"      => $this->object->work->get("title")
            ));
            $dl[translate("work location")]=$home;
        }
        return $dl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->object->getName();
    }

}
