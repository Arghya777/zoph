<?php
/**
 * View for confirm delete person
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace person\view;

use template\block;
use web\request;
use person;
use photo;
use user;

/**
 * This view displays the "confirm person" page
 */
class confirm extends view implements \view {

    protected function getActionlinks() : array {
        return array(
            translate("confirm")   =>  "person.php?_action=confirm&amp;person_id=" . $this->object->getId(),
            translate("cancel")    =>  "person.php?person_id=" . $this->object->getId()
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        $cover = $this->object->getCoverphoto();
        if ($cover instanceof photo) {
            $cover = $cover->getImageTag(THUMB_PREFIX);
        }
        return new block("confirm", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "mainActionlinks" => null,
            "obj"           => $this->object,
            "image"         => $cover
        ));

    }

    public function getTitle() : string {
        return sprintf(translate("Confirm deletion of '%s'"), $this->object->getName());
    }
}
