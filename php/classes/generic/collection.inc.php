<?php
/**
 * Generic collection class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace generic;

use ArrayIterator;

/**
 * Collection class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
abstract class collection implements \ArrayAccess, \IteratorAggregate, \Countable {
    /** @var array of items in this collection */
    protected $items=array();

    /**
     * Check if item exists
     * For ArrayAccess interface
     * @param mixed offset
     * @return bool whether or not key $off exists in items array
     */
    public function offsetExists(mixed $off) : bool {
        return isset($this->items[$off]);
    }

    /**
     * Return item
     * For ArrayAccess interface
     * @param mixed offset
     * @return mixed value of the item
     */
    public function offsetGet(mixed $off) : mixed {
        return $this->items[$off];
    }

    /**
     * Add item
     * For ArrayAccess interface
     * @param mixed offset
     * @param mixed value
     */
    public function offsetSet(mixed $off, mixed $value) : void {
        if (!is_null($off)) {
            $this->items[$off]=$value;
        } else {
            $this->items[]=$value;
        }
    }

    /**
     * Unset item (remove)
     * For ArrayAccess interface
     * @param mixed offset
     */
    public function offsetUnset(mixed $off) : void {
        unset($this->items[$off]);
    }

    /**
     * For IteratorAggregate interface
     * allow us to do foreach () on this object
     */
    public function getIterator() : ArrayIterator {
        return new ArrayIterator($this->items);
    }

    /**
     * For Countable interface
     * return size of this collection
     */
    public function count() : int {
        return count($this->items);
    }

    /**
     * Return a subset of this collection as a new collection
     * @param int start of subset
     * @param int size of subset
     */
    public function subset(int $start, ?int $count = null) : self {
        return static::createFromArray(array_slice($this->items, $start, $count, false));
    }

    /**
     * Pop last element off the collection
     * @return mixed last object of the collection
     */
    public function pop() : mixed {
        return array_pop($this->items);
    }

    /**
     * Shift first element off the collection
     * @return mixed first object of the collection
     */
    public function shift() : mixed {
        return array_shift($this->items);
    }

    /**
     * Get random element(s) from the collection
     */
    public function random(int $count = 1) : mixed {
        $count = min(sizeof($this), $count);
        if ($count > 1) {
            $rndKeys=array_rand($this->items, $count);
            shuffle($rndKeys);
        } else {
            $rndKeys = array_keys($this->items);
        }
        $rndColl = new static();
        foreach ($rndKeys as $key) {
            $rndColl[] = $this[$key];
        }
        return $rndColl;
    }

    /**
     * Merge this collection with other collection(s)
     * @param collection to merge with [, collection to merge with [ , ... ]]
     * return collection
     */
    public function merge(self ...$toMerge) : self {
        $merged=array();
        array_unshift($toMerge, $this);
        foreach ($toMerge as $collection) {
            $merged=array_merge($merged, $collection->toArray());
        }
        return static::createFromArray($merged);
    }

    /**
     * Renumber the items so that each item has it's key as
     * it's key in the array
     * @param callable alternate function to determine key (default ->getId() )
     * @return collection
     */
    public function renumber(callable $function = null) : self {
        // Default for callable can only be null
        if (!$function) {
            $function="getId";
        }
        $return = new static;
        foreach ($this->items as $item) {
            $id = call_user_func(array($item, $function));
            $return[$id]=$item;
        }
        return $return;
    }

    /**
     * Turn this collection into an array
     */
    public function toArray() : array {
        return $this->items;
    }

    /**
     * Create a new collection from an array
     * @param array Items to put in new collection
     */
    public static function createFromArray(array $items, bool $withKeys = false) : self {
        $collection = new static();
        if ($withKeys) {
            foreach ($items as $item) {
                $collection[$item->getId()]=$item;
            }
        } else {
            $collection->items = $items;
        }
        return $collection;
    }
}
