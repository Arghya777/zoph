<?php
/**
 * View for display photos page (get photos JSON data)
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use conf\conf;
use photo\collection as photoCollection;
use template\block;
use template\template;
use web\request;

use user;

/**
 * This view returns photos as JSON data
 */
class json extends view {
    /**
     * Output view
     */
    public function view() : string {
        $json = array();

        foreach ($this->display as $photo) {
            $json[] = $photo->getId();
        }

        return json_encode($json);
    }

    public function getHeaders() : array {
        return array("Content-Type: application/json");
    }

    public function display($template = "json") : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }
        echo $this->view();
    }
}
