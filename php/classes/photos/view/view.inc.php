<?php
/**
 * Common parts for photos view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photos\view;

use search;
use user;

use conf\conf;
use photo\collection as photoCollection;
use photos\params;
use web\request;
use web\view\view as webView;

/**
 * This is a base view for the photos page, that contains common parts for all views
 */
abstract class view extends webView implements \view {

    /* @var photo\collection photos for this request */
    protected $photos;

    /** @var photo\collection photos to display */
    protected $display;

    /** @var array of photo_ids that are in this user's lightbox */
    protected $lightbox;

    /** * @var array request variables */
    protected $vars;

    /**
     * Create view
     */
    public function __construct(protected request $request, protected params $params) {
    }

    /**
     * Get actionlinks
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();
        $qs=$this->request->cleanQueryString(array("/_crumb=\d+&?/", "/_action=\w+&?/"));
        $actionlinks=array();
        if ($this->request["_action"] ?? "display" == "search") {
            $search = new search();
            $search->setSearchURL($this->request);
            $actionlinks[translate("save search")] = "search.php?_action=new&_qs=" . $search->getSearchQS();
            $qs = urldecode($search->getSearchQS());
        }
        if ($user->isAdmin()) {
            $actionlinks[translate("edit")] = "photos.php?_action=edit&" . $qs;
            $actionlinks[translate("geotag")] = "track.php?_action=geotag&" . $qs;
        }

        $actionlinks[translate("slideshow")] = "slideshow.php?" . $qs;

        if (conf::get("feature.download") && ($user->get("download") || $user->isAdmin())) {
            $actionlinks[translate("download")] = "photos.php?_action=download&" . $qs;
        }

        if (conf::get("feature.sets") && $user->isAdmin() && !isset($this->request["set_id"])) {
            $actionlinks[translate("add to set")] = "set.php?_action=choose&" . $qs;
        }
        return $actionlinks;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() :string {
        return $this->params->title;
    }

   /**
    * Set photo collection
    * @param photo\collection photos for this view
    */
    public function setPhotos(photoCollection $photos) : void {
        $this->photos = $photos;
    }

   /**
    * Set photo collection to be displayed
    * @param photo\collection photos to display
    */
    public function setDisplay(photoCollection $display) : void {
        $this->display = $display;
    }

   /**
    * Set photoids for the lightbox of the current user
    * @param array|null of photoids
    */
    public function setLightbox(?array $lightbox) : void {
        $this->lightbox = (array) $lightbox;
    }
}
