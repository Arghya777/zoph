<?php
/**
 * Class that takes care of displaying a full page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace template;

use breadcrumb;
use conf\conf;
use language;
use user;

/**
 * This class takes care of displaying a page {
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class page extends template {
    public function __construct($template = "html", $vars=null) {
        if (!defined("INSTALL")) {
            $vars = array_merge($this->getVars(), $vars);
        }
        parent::__construct($template, $vars);
    }

    private function getVars() {
        $vars=array(
            "lang"          => language::getCurrentISO(),
            "html_class"    => "",
            "icons"         => $this->getIcons(),
            "scripts"       => $this->getScripts(),
            "javascripts"   => $this->getJavascripts(),
            "favicon"       => static::getImage("icons/favicon.png"),
            "template"      => conf::get("interface.template"),
            "menu"          => new menu(),
            "breadcrumbs"   => breadcrumb::display()
        );

        if (isset($prev_url)) {
            $vars["next"] = $prev_url;
        }
        if (isset($next_url)) {
            $vars["next"] = $next_url;
        }

        return $vars;
    }

    private function getIcons() {
        return array(
            "count"     => template::getImage("icons/photo.png"),
            "taken"     => template::getImage("icons/date.png"),
            "modified"  => template::getImage("icons/modified.png"),
            "rated"     => template::getImage("icons/rating.png"),
            "children"  => template::getImage("icons/folder.png"),
            "geo-photo" => template::getImage("icons/geo-photo.png"),
            "geo-place" => template::getImage("icons/geo-place.png"),
            "pause"     => template::getImage("icons/pause.png"),
            "play"      => template::getImage("icons/play.png"),
            "resize"    => template::getImage("icons/resize.png"),
            "unpack"    => template::getImage("icons/unpack.png"),
            "remove"    => template::getImage("icons/remove.png"),
            "down2"     => template::getImage("down2.gif"),
            "pleasewait"=> template::getImage("pleasewait.gif")
        );
    }

    /**
     * @todo needs to move to each file individually
     */
    private function getScripts() {
        $scripts=array(
            "js/util.js",
            "js/xml.js",
            "js/thumbview.js",
            "js/translate.js",
            "js/error.js"
        );

        switch(basename($_SERVER["SCRIPT_NAME"])) {
        case "import.php":
            $scripts[]="js/import.js";
            $scripts[]="js/formhelper.js";
            $scripts[]="js/rating.js";
            if (conf::get("import.upload")) {
                $scripts[]="js/upload.js";
            }
            break;
        case "config.php":
            $scripts[]="js/conf.js";
            break;
        case "photo.php":
            $scripts[]="js/photoPeople.js";
            $scripts[]="js/rating.js";
        case "photos.php":
            $scripts[]="js/set.js";
        case "place.php":
            $scripts[]="js/json.js";
            $scripts[]="js/locationLookup.js";
            break;
        case "slideshow.php":
            $scripts[]="js/json.js";
            $scripts[]="js/slideshow.js";
            break;
        case "user.php":
            $scripts[]="js/password.js";
            break;
        case "zoph.php":
            $scripts[]="js/feature.js";
            break;
        }

        if (conf::get("interface.autocomplete")) {
            $scripts[]="js/autocomplete.js";
        }

        if (conf::get("maps.provider")) {
            $scripts[]="js/leaflet-src.js";
            $scripts[]="js/maps.js";

            if (conf::get("maps.geocode")) {
                $scripts[]="js/geocode.js";
            }
        }

        return $scripts;
    }

    private function getJavascripts() {
        $javascript = array();
        if (conf::get("maps.provider") == "mapbox") {
            $javascript[]="var mapbox_api_key = '" . conf::get("maps.mapbox.apikey") . "';";
        }
        return $javascript;
    }

}
