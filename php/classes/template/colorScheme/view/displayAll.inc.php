<?php
/**
 * View for display All colour schemes page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use conf\conf;
use template\colorScheme;
use template\block;
use web\request;

use user;

/**
 * This view displays the All colorSchemes page
 */
class displayAll extends view implements \view {
    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array(
            translate("new")    => "color_scheme.php?_action=new"
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl=new block("displayColorSchemes", array(
            "title" => $this->getTitle(),
            "cs"    => colorScheme::getRecords("name")
        ));
        $tpl->addActionlinks($this->getActionlinks());
        return $tpl;

    }

    public function getTitle() : string {
        return translate("Color Schemes");
    }
}
