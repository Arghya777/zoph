<?php
/**
 * View for confirm delete color scheme
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme\view;

use template\block;
use template\colorScheme;
use web\request;

/**
 * This view displays the "confirm color scheme" page
 */
class confirm extends view implements \view {

    protected function getActionlinks() : array {
        return array(
            translate("confirm")   =>  "color_scheme.php?_action=confirm&amp;color_scheme_id=" .
                $this->object->getId(),
            translate("cancel")    =>  "color_scheme.php?_action=displayAll"
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => null,
            "mainActionlinks"   => $this->getActionlinks(),
            "obj"               => $this->object,
        ));

    }

    public function getTitle() : string {
        return translate("Confirm delete ") . $this->object->getName();
    }
}
