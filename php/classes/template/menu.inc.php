<?php
/**
 * Class that takes care of displaying the menu
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace template;

use conf\conf;
use user;
/**
 * This class takes care of displaying the menu
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class menu extends block {
    public function __construct($template = "menu") {
        if (strpos($_SERVER["PHP_SELF"], "/") === false) {
            $self = $_SERVER["PHP_SELF"];
        } else {
            $self = substr(strrchr($_SERVER['PHP_SELF'], "/"), 1);
        }

        parent::__construct($template, array(
            "tabs"  => $this->getTabs(),
            "self"  => $self
        ));

    }

    public function getTabs() {
        $user = user::getCurrent();
        $tabs = array(
            translate("home", 0) => "zoph.php",
            translate("albums", 0) => "album.php",
            translate("categories", 0) => "category.php"
        );
        if ($user?->canBrowsePeople()) {
            $tabs[translate("people", 0)] = "person.php?_action=people";
        }

        if ($user?->canBrowsePlaces()) {
            $tabs[translate("places", 0)] = "place.php";
        }

        if (conf::get("feature.sets") && $user?->isAdmin()) {
            $tabs[translate("sets", 0)] = "set.php?_action=sets";
        }

        $tabs[translate("photos", 0)] = "photos.php";

        if ($user?->get("lightbox_id")) {
            $tabs[translate("lightbox", 0)] = "photos.php?album_id=" .
                $user->get("lightbox_id");
        }

        $tabs[translate("search",0)] = "search.php";

        if (conf::get("import.enable") && ($user?->isAdmin() || $user?->get("import"))) {
            $tabs[translate("import", 0)] = "import.php";
        }

        if ($user?->isAdmin()) {
            $tabs[translate("admin", 0)] = "admin.php";
        }

        $tabs[translate("reports", 0)]  = "zoph.php?_action=reports";
        if ($user?->get("user_id") != conf::get("interface.user.default")) {
            $tabs[translate("prefs", 0)] = "user.php?_action=prefs";
        }
        $tabs[translate("about", 0)] = "zoph.php?_action=info";

        if ($user?->get("user_id") == conf::get("interface.user.default")) {
            $tabs[translate("logon", 0)] = "zoph.php?_action=logout";
        } else {
            $tabs[translate("logout", 0)] = "zoph.php?_action=logout";
        }
        return $tabs;
    }
}
