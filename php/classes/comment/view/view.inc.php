<?php
/**
 * common parts of view for comment
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use conf\conf;
use template\block;
use template\template;
use web\request;
use web\view\view as webView;

use comment;
use photo;
use user;

/**
 * Common parts for comment views
 */
abstract class view extends webView implements \view {
    protected $photo;

    public function __construct(protected request $request, protected array|comment $object) {
        if (!is_array($this->object)) {
            $photo = $this->object->getPhoto();
            if (!$photo instanceof photo && $this->object->getId() == 0) {
                $photo = new photo((int) $request["photo_id"]);
                $photo->lookup();
            }
            $this->photo=$photo;
        }
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();
        $actionlinks=array(
            translate("return")    =>  "photo.php?photo_id=" . $this->photo->getId()
        );

        if ($this->object->getId() != 0 && ($user->isAdmin() || $this->object->isOwner($user))) {
            $actionlinks = array_merge($actionlinks, array(
                translate("edit")      =>  "comment.php?_action=edit&amp;comment_id=" . $this->object->getId(),
                translate("delete")    =>  "comment.php?_action=delete&amp;comment_id=" . $this->object->getId(),
            ));
        }
        return $actionlinks;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->object->get("subject");
    }
}
