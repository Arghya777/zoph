<?php
/**
 * View for edit comments
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use conf\conf;
use template\block;
use template\form;
use web\request;
use zophCode\smiley;

use comment;
use photo;
use user;

/**
 * This view displays the comment page when editing
 */
class update extends view implements \view {
    /**
     * Output the view
     */
    public function view() : block {
        $user = user::getCurrent();
        $photo=$this->photo;

        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if ($this->request["_action"] == "edit") {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
        }

        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock($photo->getImageTag(MID_PREFIX));

        $form = new form("form", array(
            "formAction"    => "comment.php",
            "class"         => "comment",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate($action, 0)
        ));


        $form->addInputHidden("comment_id", $this->object->getId());
        $form->addInputHidden("photo_id", $this->photo->getId());
        $form->addInputText("subject", $this->object->get("subject"), translate("subject"));
        $form->addTextarea("comment", $this->object->get("comment"), translate("comment"), 80, 8);


        $tpl->addBlock($form);
/*
         <h2><?php echo translate("smileys you can use"); ?></h2>
*/
       $tpl->addBlock(smiley::getOverview());

        return $tpl;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        if ($this->object->getId()==0) {
            return translate("Add comment");
        } else if (trim($this->object->get("subject"))=="") {
            return translate("comment");
        } else {
            return $this->object->get("subject");
        }
    }

}
