<?php
/**
 * View for confirm delete comment
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use template\block;
use web\request;
use comment;
use photo;
use user;

/**
 * This view displays the "confirm comment" page
 */
class confirm extends view implements \view {

   /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array(
            translate("confirm")   =>  "comment.php?_action=confirm&amp;comment_id=" . $this->object->getId(),
            translate("cancel")    =>  "comment.php?comment_id=" . $this->object->getId()
        );
    }
    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => null,
            "mainActionlinks"   => $this->getActionlinks(),
            "obj"               => $this->object,
        ));

    }

    public function getTitle() : string {
        $subject = $this->object->get("subject");
        $commentUser = new user($this->object->get("user_id"));
        $commentUser->lookup();
        $name=$commentUser->get("user_name");
        return sprintf(translate("Confirm deletion of comment '<b>%s</b>' by '<b>%s</b>'"), $subject, $name);
    }
}
