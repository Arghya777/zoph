<?php
/**
 * Controller for pages
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace page;

use conf\conf;
use generic\controller as genericController;
use web\request;
use page;
use pageset;
use user;

/**
 * Controller for page
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewPages     = view\pages::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert", "new", "pages", "update"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);

        $pageId = $request["page_id"] ?? null;

        $this->object = new page($pageId);
        $this->object->lookup();

        $this->doAction();
    }

    /**
     * Do action 'confirm'
     */
    protected function actionConfirm() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionConfirm();
            $this->view->setRedirect("page.php?_action=pages");
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'delete'
     */
    protected function actionDelete() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            parent::actionDelete();
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'edit'
     */
    protected function actionEdit() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->view = new static::$viewDisplay($this->request, $this->object);
        }
    }

    /**
     * Do action 'pages'
     */
    protected function actionPages() {
        $this->view = new static::$viewPages($this->request);
    }


    /**
     * Do action 'update'
     */
    protected function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin()) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
        }
        $this->view = new static::$viewDisplay($this->request, $this->object);
    }

    /**
     * Do action 'insert'
     */
    protected function actionInsert() {
        $user = user::getCurrent();
        if ($user->isAdmin()) {
            $page = new page();
            $this->setObject($page);
            parent::actionInsert();
        }
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $this->view->setRedirect("page.php?page_id=" . $this->object->getId());
    }
}
