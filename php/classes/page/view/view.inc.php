<?php
/**
 * common parts of view for pageset
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace page\view;

use conf\conf;
use page;
use template\block;
use template\template;
use user;
use web\request;
use web\view\view as webView;

/**
 * Common parts for page views
 */
abstract class view extends webView implements \view {

    public function __construct(protected request $request, protected ?page $object = null) {
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();
        $actionlinks=array(
            translate("return")    =>  "page.php?_action=pages"
        );

        if ($this->object->getId() != 0 && $user->isAdmin()) {
            $actionlinks = array_merge($actionlinks, array(
                translate("edit")      =>  "page.php?_action=edit&amp;page_id=" . $this->object->getId(),
                translate("delete")    =>  "page.php?_action=delete&amp;page_id=" . $this->object->getId(),
            ));
        }
        return $actionlinks;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->object->get("title");
    }
}
