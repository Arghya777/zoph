<?php
/**
 * View for display photo page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use calendar\model as calendar;
use conf\conf;
use geo\map;
use photo;
use photo\people as photoPeople;
use rating;
use selection;
use template\block;
use template\template;
use Time;
use user;
use web\request;

/**
 * This view displays the "photo" page
 */
class display extends view {

    /**
     * Get comments for the current photo
     * @return block template block
     */
    private function getComments() : ?block {
        if (conf::get("feature.comments")) {
            $comments=$this->photo->getComments();
            if ($comments) {
                $commentTpl=new block("comments", array(
                    "comments"  => $comments
                ));
            }
        }
        return $commentTpl ?? null;
    }

    /**
     * Get related photos for the current photo
     * @return block template block
     */
    private function getRelated() : ?block {
        $related=$this->photo->getRelated();
        if ($related) {
            $tplRelated=new block("related_photos", array(
                "photo"     => $this->photo,
                "related"   => $related,
                "admin"     => (bool) user::getCurrent()->isAdmin()
            ));
        }
        return isset($tplRelated) ? $tplRelated : null;
    }

    /**
     * Get EXIF information for current photo
     * @return block template block
     */
    private function getExif() : ?block {
        if (user::getCurrent()->prefs->get("allexif")) {
            $exif=new block("exif", array(
                "allexif"   => $this->photo->exifToHTML()
            ));
        }
        return isset($exif) ? $exif : null;
    }

    /**
     * Get rating block (display) for current photo
     * @return block template block
     */
    private function getRating() : null|block|float {
        $rating = $this->photo->getRating();
        if ($rating && user::getCurrent()->isAdmin()) {
            $rating=$this->photo->getRatingDetails();
        }
        return $rating;
    }

    /**
     * Get rating block (to rate) for current photo
     * @return block template block
     */
    private function getRatingForm() : ?block {
        $user = user::getCurrent();
        if (conf::get("feature.rating") && $user->canRatePhotos()) {
            $ratingForm=new block("formRating", array(
                "ratingDropdown"=> rating::createDropdown("rating", $this->photo->getRatingForUser($user)),
                "photoId"       => $this->photo->getId()
            ));
        }
        return isset($ratingForm) ? $ratingForm : null;
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $actionlinks=parent::getActionlinks();
        unset($actionlinks[translate("cancel")]);
        unset($actionlinks[translate("return")]);
        return $actionlinks;
    }


    /**
     * Output view
     */
    public function view() : block {
        $user = user::getCurrent();
        $photo = $this->photo;
        $photo->lookup();

        if (!$user->isAdmin()) {
            $permissions = $user->getPhotoPermissions($photo);
        }

        $this->setLinks();

        $camInfo = $user->prefs->get("camera_info") ? $photo->getCameraDisplayArray() : null;

        $calendar = new calendar();
        $calendar->setSearchField("timestamp");

        if ($user->canBrowsePeople()) {
            $people=(new photoPeople($photo))->getInRows();
        }

        $timestamp = new Time($photo->get("timestamp"));

        if (conf::get("maps.provider")) {
            $map = new map();
            $photos=$photo->getNear(100);
            $photos[]=$photo;
            $map->addMarkers($photos, $user);
        }
        $prevnext = new block("photoPrevNext", array(
            "prev"          => $this->prevURL,
            "up"            => $this->upURL,
            "next"          => $this->nextURL
        ));

        return new block("displayPhoto", array(
            "photo"         => $photo,
            "actionlinks"   => $this->getActionlinks(),
            "title"         => $this->getTitle(),
            "selection"     => $this->getSelection(),
            "prevnext"      => $prevnext,
            "full"          => $full=$photo->getFullsizeLink($photo->get("name")),
            "size"          => template::getHumanReadableBytes((int)$photo->get("size")),
            "share"         => $this->getShare(),
            "image"         => $photo->getFullsizeLink($photo->getImageTag(MID_PREFIX)),
            "people"        => $people ?? null,
            "fields"        => $photo->getDisplayArray(),
            "rating"        => $this->getRating(),
            "ratingForm"    => $this->getRatingForm(),
            "albums"        => template::createLinkList($photo->getAlbums()),
            "categories"    => template::createLinkList($photo->getCategories()),
            "timestampURL"  => $calendar->getDateLink($timestamp),
            "timestamp"     => $timestamp->getFormatted(),
            "description"   => trim($photo->get("description")),
            "camInfo"       => $camInfo,
            "related"       => $this->getRelated(),
            "exifdetails"   => $this->getExif(),
            "comments"      => $this->getComments(),
            "map"           => $map ?? null
        ));
    }
}
