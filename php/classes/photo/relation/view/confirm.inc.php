<?php
/**
 * View for confirm delete photo\relation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\relation\view;

use photo;
use template\block;
use template\template;

/**
 * This view displays the "confirm photo\relation" page
 */
class confirm extends view implements \view {

   /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array(
            translate("confirm")   =>  "relation.php?_action=confirm&amp;" . $this->getIds(),
            translate("cancel")    =>  "relation.php?" . $this->getIds()
        );
    }
    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             => $this->getTitle(),
            "confirm"           => translate("delete relationship"),
            "actionlinks"       => null,
            "mainActionlinks"   => $this->getActionlinks(),
            "image"             => $this->getRelationBlock(),
            "obj"               => $this->object
        ));

    }

    public function getTitle() : string {
        return translate("delete relationship");
    }
}
