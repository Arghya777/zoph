<?php
/**
 * View for edit photo\relations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\relation\view;

use template\form;
use template\block;
use web\request;

use photo;

/**
 * This view displays the photo\relation page when editing
 */
class update extends view implements \view {
    /**
     * Output the view
     */
    public function view() : block {
        $photo1 = new photo($this->object->get("photo_id_1"));
        $photo2 = new photo($this->object->get("photo_id_2"));

        if ($this->request["_action"] == "new") {
            $action = "insert";
            $desc_1 = "";
            $desc_2 = "";
        } else if ($this->request["_action"] == "edit" || $this->request["_action"] == "update") {
            $desc_1 = $this->object->getDesc($photo1);
            $desc_2 = $this->object->getDesc($photo2);
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $desc_1 = "";
            $desc_2 = "";
            $action = $this->request["_action"];
        }

        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock($this->getRelationBlock());

        $form = new form("form", array(
            "formAction"    => "relation.php",
            "class"         => "relation",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate($action, 0)
        ));

        $form->addInputHidden("photo_id_1", $photo1->getId());
        $form->addInputHidden("photo_id_2", $photo2->getId());
        $form->addInputText("desc_1", $desc_1, translate("Description for first photo"));
        $form->addInputText("desc_2", $desc_2, translate("Description for second photo"));

        $tpl->addBlock($form);

        return $tpl;
    }

}
