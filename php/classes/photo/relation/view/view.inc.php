<?php
/**
 * common parts of view for photo\relation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\relation\view;

use conf\conf;
use template\block;
use web\request;
use web\view\view as webView;

use photo\relation\model as photoRelation;
use photo;
use user;

/**
 * Common parts for photo\relation views
 */
abstract class view extends webView implements \view {

    public function __construct(protected request $request, protected photoRelation $object) {
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {

        if ($this->object->getId() != 0) {
            $actionlinks = array(
                translate("edit")      =>  "relation.php?_action=edit&amp;" . $this->getIds(),
                translate("delete")    =>  "relation.php?_action=delete&amp;" . $this->getIds()
            );
        } else {
            $actionlinks = array();
        }
        return $actionlinks;
    }

    protected function getIds() {
        return "photo_id_1=" . $this->object->get("photo_id_1") . "&amp;photo_id_2=" . $this->object->get("photo_id_2");
    }

    protected function getRelationBlock() {
        $photo1 = new photo($this->object->get("photo_id_1"));
        $photo2 = new photo($this->object->get("photo_id_2"));
        return new block("relation", array(
            "photo1"            => $photo1->getImageTag(THUMB_PREFIX),
            "photo2"            => $photo2->getImageTag(THUMB_PREFIX),
            "desc1"             => $this->object->getDesc($photo1),
            "desc2"             => $this->object->getDesc($photo2),
        ));
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate($this->request["action"] . " relationship");
    }
}
