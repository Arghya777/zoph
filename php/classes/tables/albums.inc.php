<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class albums extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("albums");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("album_id"))->int()->notNull()->autoIncrement()->setPrimaryKey(),
            (new column("parent_album_id"))->int()->notNull()->default("0")->setKey("album_parent_id"),
            (new column("album"))->varchar(64)->notNull()->default(''),
            (new column("album_description"))->varchar(255)->default("NULL"),
            (new column("sortname"))->char(32),
            (new column("coverphoto"))->int()->default("NULL"),
            (new column("pageset"))->int()->default("NULL"),
            (new column("sortorder"))->varchar(32)->default("NULL"),
            (new column("createdby"))->int()->notNull()->default(1)
        ));

        return $table;
    }

    protected function data() : array {
        $return = array();
        $qry = new select("albums");
        $qry->addFunction(array("count" => "COUNT(*)"));
        $qry->where(new clause("album_id=:albumid"));
        $qry->addParam(new param(":albumid", 1, PDO::PARAM_INT));

        if ($qry->getCount() == 0) {
            $qry = new insert("albums");
            $qry->addParam(new param(":album_id",1, PDO::PARAM_INT));
            $qry->addParam(new param(":album", "Album Root", PDO::PARAM_STR));

            $return=array($qry);
        }
        return $return;
    }
}
