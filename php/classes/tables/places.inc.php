<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class places extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("places");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("place_id"))->int()->notNull()->autoIncrement()->setPrimaryKey(),
            (new column("parent_place_id"))->int()->notNull(),
            (new column("title"))->varchar(64)->notNull()->default("")->setKey("place_title", 10),
            (new column("address"))->varchar(64)->default("NULL"),
            (new column("address2"))->varchar(64)->default("NULL"),
            (new column("city"))->varchar(32)->default("NULL")->setKey("place_city", 10),
            (new column("state"))->varchar(32)->default("NULL"),
            (new column("zip"))->varchar(10)->default("NULL"),
            (new column("country"))->varchar(32)->default("NULL"),
            (new column("url"))->varchar(1024)->default("NULL"),
            (new column("urldesc"))->varchar(32)->default("NULL"),
            (new column("coverphoto"))->int()->default("NULL"),
            (new column("pageset"))->int()->default("NULL"),
            (new column("notes"))->varchar(255)->default("NULL"),
            (new column("lat"))->float(10,6),
            (new column("lon"))->float(10,6),
            (new column("mapzoom"))->tinyint()->unsigned(),
            (new column("timezone"))->varchar(50)->default("NULL"),
            (new column("createdby"))->int()->notNull()->default(1)
        ));
        return $table;
    }

    protected function data() : array {
        $return = array();
        $qry = new select("places");
        $qry->addFunction(array("count" => "COUNT(*)"));
        $qry->where(new clause("place_id=:placeid"));
        $qry->addParam(new param(":placeid", 1, PDO::PARAM_INT));

        if ($qry->getCount() == 0) {

            $qry = new insert("places");
            $qry->addParam(new param(":place_id",1, PDO::PARAM_INT));
            $qry->addParam(new param(":parent_place_id",0, PDO::PARAM_INT));
            $qry->addParam(new param(":title", "World", PDO::PARAM_STR));

            $return=array($qry);
        }
        return $return;
    }
}
