<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use db\column;
use db\create;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class photos extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("photos");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("photo_id"))->int()->notNull()->autoIncrement()->setPrimaryKey(),
            (new column("name"))->varchar(128)->default("NULL"),
            (new column("path"))->varchar(255)->default("NULL"),
            (new column("width"))->int()->default("NULL"),
            (new column("height"))->int()->default("NULL"),
            (new column("size"))->int()->default("NULL"),
            (new column("title"))->varchar(64)->default("NULL"),
            (new column("photographer_id"))->int()->default("NULL")->setKey("photo_photog_id"),
            (new column("location_id"))->int()->default("NULL")->setKey("photo_loc_id"),
            (new column("view"))->varchar(64)->default("NULL"),
            (new column("description"))->text(),
            (new column("date"))->varchar(10)->default("NULL"),
            (new column("time"))->varchar(8)->default("NULL"),
            (new column("time_corr"))->smallint()->notNull()->default(0),
            (new column("camera_make"))->varchar(32)->default("NULL"),
            (new column("camera_model"))->varchar(32)->default("NULL"),
            (new column("flash_used"))->char(1)->default("NULL"),
            (new column("focal_length"))->varchar(64)->default("NULL"),
            (new column("exposure"))->varchar(64)->default("NULL"),
            (new column("compression"))->varchar(64)->default("NULL"),
            (new column("aperture"))->varchar(16)->default("NULL"),
            (new column("level"))->tinyint()->notNull()->default(1)->setKey("photo_level"),
            (new column("iso_equiv"))->varchar(8)->default("NULL"),
            (new column("metering_mode"))->varchar(16)->default("NULL"),
            (new column("focus_dist"))->varchar(16)->default("NULL"),
            (new column("ccd_width"))->varchar(16)->default("NULL"),
            (new column("comment"))->varchar(128)->default("NULL"),
            (new column("timestamp"))->timestamp()->notNull()->default("CURRENT_TIMESTAMP")->onUpdate("CURRENT_TIMESTAMP"),
            (new column("lat"))->float(10,6),
            (new column("lon"))->float(10,6),
            (new column("mapzoom"))->tinyint()->unsigned(),
            (new column("hash"))->char(40)->default("NULL"),
            (new column("imported"))->timestamp()->notNull()->default("CURRENT_TIMESTAMP")
        ));

        return $table;
    }

}
