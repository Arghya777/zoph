<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class people extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("people");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("person_id"))->int()->notNull()->autoIncrement()->setPrimaryKey(),
            (new column("first_name"))->varchar(32)->default("NULL")->setKey("person_last_name", 10),
            (new column("last_name"))->varchar(32)->default("NULL")->setKey("person_last_name", 10),
            (new column("middle_name"))->varchar(32)->default("NULL"),
            (new column("called"))->varchar(16)->default("NULL"),
            (new column("gender"))->char(1)->default("NULL"),
            (new column("dob"))->date()->default("NULL"),
            (new column("dod"))->date()->default("NULL"),
            (new column("sortname"))->char(32),
            (new column("home_id"))->int()->default("NULL"),
            (new column("work_id"))->int()->default("NULL"),
            (new column("father_id"))->int()->default("NULL"),
            (new column("mother_id"))->int()->default("NULL"),
            (new column("spouse_id"))->int()->default("NULL"),
            (new column("notes"))->varchar(255)->default("NULL"),
            (new column("coverphoto"))->int()->default("NULL"),
            (new column("pageset"))->int()->default("NULL"),
            (new column("email"))->varchar(64)->default("NULL"),
            (new column("createdby"))->int()->notNull()->default(1)
        ));

        return $table;
    }

    protected function data() : array {
        $return = array();
        $qry = new select("people");
        $qry->addFunction(array("count" => "COUNT(*)"));
        $qry->where(new clause("person_id=:personid"));
        $qry->addParam(new param(":personid", 1, PDO::PARAM_INT));

        if ($qry->getCount() == 0) {

            $qry = new insert("people");
            $qry->addParam(new param(":person_id",1, PDO::PARAM_INT));
            $qry->addParam(new param(":first_name", "Unknown", PDO::PARAM_STR));
            $qry->addParam(new param(":last_name", "Person", PDO::PARAM_STR));

            $return=array($qry);
        }
        return $return;
    }
}
