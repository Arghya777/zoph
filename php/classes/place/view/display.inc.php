<?php
/**
 * View to display places
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use conf\conf;
use geo\map;
use geo\marker;
use organiserView;
use pageException;
use photoNoSelectionException;
use place;
use selection;
use template\block;
use user;
use web\request;

/**
 * Display screen for placess
 */
class display extends view implements \view {
    use organiserView;

    private const VIEWNAME = "Place View";
    private const PHOTOLINK = "photos.php?location_id=";
    private const DESCRIPTION = "place_description";

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();
        $actionlinks=array();

        if ($user->canEditOrganizers()) {
            $actionlinks=array(
                translate("edit") => "place.php?_action=edit&amp;place_id=" . (int) $this->object->getId(),
                translate("new") => "place.php?_action=new&amp;parent_place_id=" . (int) $this->object->getId(),
                translate("delete") => "place.php?_action=delete&amp;place_id=" . (int) $this->object->getId()
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks["unset coverphoto"]="place.php?_action=unsetcoverphoto&amp;place_id=" . (int) $this->object->getId();
            }
        }
        return $actionlinks;
    }

    private function getSelection() : ?selection {
        try {
            $selection=new selection($_SESSION, array(
                "coverphoto"    => "place.php?_action=coverphoto&amp;place_id=" . $this->object->getId() . "&amp;coverphoto=",
                "return"        => "_return=place.php&amp;_qs=place_id=" . $this->object->getId()
            ));
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }

        return $selection;    
    }
    
    public function getTitle() : string {
        return $this->object->get("parent_place_id") ? $this->object->get("title") : translate("Places");
    }

    protected function getMap() : ?map {
        if (conf::get("maps.provider")) {
            $map=new map();
            $map->setCenterAndZoomFromObj($this->object);
            $marker=$this->object->getMarker();
            if ($marker instanceof marker) {
                $map->addMarker($marker);
            }
            $map->addMarkers($this->object->getChildren());
            return $map;
        }
        return null;
    }
}
