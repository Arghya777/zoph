<?php
/**
 * View for 'place not found' page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use web\request;
use template\block;

/**
 * This view displays a "not found" error in case no place was found
 * or the user is not allowed to see it
 */
class notfound extends view {
    /**
     * Create view
     * @param request web request
     */
    public function __construct(request $request) {
        $this->request=$request;
        $this->vars=$request->getRequestVars();
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("notFound", array(
            "title" => $this->getTitle(),
            "msg"   => translate("Place not found")
        ));
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Place not found");
    }

}
