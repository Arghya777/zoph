<?php
/**
 * View for confirm delete place
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use template\block;
use web\request;
use photo;
use user;
use place;

/**
 * This view displays the "confirm delete place" page
 */
class confirm extends view {


    /**
     * Get actionlinks
     */
    public function getActionlinks() : array {
        return array(
            translate("confirm")   =>  "place.php?_action=confirm&amp;place_id=" . $this->object->getId(),
            translate("cancel")    =>  "place.php?place_id=" . $this->object->getId()
        );

    }
    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"     =>  translate("delete place"),
            "actionlinks"   => $this->getActionlinks(),
            "mainActionlinks" => null,
            "obj"           => $this->object,
        ));

    }
}
