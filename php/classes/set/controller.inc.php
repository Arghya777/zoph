<?php
/**
 * Controller for sets
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set;

use conf\conf;
use generic\controller as genericController;
use web\request;

use log;
use set\model as set;
use photo;
use photos\params;
use photo\collection as photoCollection;
use user;

use organiserActions;

/**
 * Controller for set
 */
class controller extends genericController {
    use organiserActions;

    protected static $viewChoose    = view\choose::class;
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewSets      = view\sets::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewMove      = \photos\view\json::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "addphoto", "addphotos", "choose", "new", "insert", "confirm", "delete", "display", "edit", "move", "removephoto", "update", "coverphoto", "unsetcoverphoto", "sets"
    );

    /**
     * Create a controller using a web request
     * @param request request
     */
    public function __construct(request $request) {
        parent::__construct($request);
        if (in_array($request->_action, [ "new", "insert" ])) {
            $set=new set();
            $this->setObject($set);
            $this->doAction();
        } else if (in_array($request->_action, [ "sets", "choose" ])) {
            $this->doAction();
        } else {
            $set=$this->getSetFromRequest();
            if ($set instanceof set) {
                $this->setObject($set);
                $this->doAction();
            } else {
                $this->view = new static::$viewNotfound($this->request, $this->object);
            }
        }
    }

    /**
     * Get the set based on the query in the request
     */
    private function getSetFromRequest() {
        $user=user::getCurrent();
        if ($user->isAdmin()) {
            $set = null;
            if (isset($this->request["set_id"])) {
                $set = new set( (int) $this->request["set_id"]);
            } else if (isset($this->request["_set_id"])) {
                $set = new set( (int) $this->request["_set_id"]);
            }

            if ($set instanceof set) {
                $set->lookup();
                return $set;
            }
        }
    }

    /**
     * Do action 'addphoto'
     */
    public function actionAddPhoto() {
        if (user::getCurrent()->isAdmin()) {
            $this->object->addPhoto(new photo((int) $this->request["_photo_id"]));
        }
        $this->actionDisplay();
    }

    /**
     * Do action 'addphotos'
     */
    public function actionAddPhotos() {
        foreach (photoCollection::createFromRequest($this->request) as $photo) {
            $this->object->addPhoto($photo);
        }
        $this->actionDisplay();
    }

    /**
     * Do action 'choose'
     * Choose a set to add photos to
     */
    public function actionChoose() {
        $this->view=new static::$viewChoose($this->request);

        $this->view->setPhotos(photoCollection::createFromRequest($this->request));
    }

    /**
     * Do action 'coverphoto'
     */
    public function actionCoverphoto() {
        if (user::getCurrent()->isAdmin()) {
            $this->object->set("coverphoto", (int) $this->request["coverphoto"]);
            $this->object->update();
        }
        $this->actionDisplay();
    }

    /**
     * Do action 'removehoto'
     */
    public function actionRemovephoto() {
        if (user::getCurrent()->isAdmin()) {
            $this->object->removePhoto(new photo((int) $this->request["_photo_id"]));
        }
        $this->view = new static::$viewRedirect($this->request, $this->object);
        $this->view->setRedirect("photos.php?" . $this->request->getPassedQueryString());
    }

    /**
     * Do action 'unsetcoverphoto'
     */
    public function actionUnsetcoverphoto() {
        if (user::getCurrent()->isAdmin()) {
            $this->object->set("coverphoto", null);
            $this->object->update();
        }
        $this->actionDisplay();
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->isAdmin()) {
            parent::actionConfirm();
            $this->view->setRedirect("set.php?_action=sets");
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->isAdmin()) {
            parent::actionDelete();
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        $user = user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'insert'
     */
    public function actionInsert() {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

    public function actionMove() {

        $photo = new photo((int) $this->request["_photoId"]);
        $target = new photo((int) $this->request["_targetId"]);
        $set = new set((int) $this->request["set_id"]);

        $set->movePhoto($photo, $target);

        $params = new params($this->request);
        $photos = $set->getPhotos();

        $this->view=new static::$viewMove($this->request, $params);
        $this->view->setPhotos($photos);
        $this->view->setDisplay($photos->subset($params->offset, $params->cells));
    }

    /**
     * Do action 'new'
     */
    public function actionNew() {
        $user = user::getCurrent();
        if ($user->canEditOrganizers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'sets'
     */
    public function actionSets() {
        $this->view = new static::$viewSets($this->request);
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }
}
