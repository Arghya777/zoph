<?php
/**
 * View for display set
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use selection;
use template\block;
use set\model as set;
use user;
use photoNoSelectionException;

/**
 * This view displays set
 */
class display extends view implements \view {

    public function getActionlinks() : array {
        return array(
            translate("new") => "set.php?_action=new",
            translate("edit") => "set.php?_action=edit&set_id=" . $this->object->getId(),
            translate("delete") => "set.php?_action=delete&set_id=" . $this->object->getId(),
            translate("return") => "set.php?_action=sets"
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
            "selection"         => $this->getSelection()
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(new block("definitionlist", array(
            "class" => "display set",
            "dl"    => $this->object->getDisplayArray()
        )));

        $tpl->addBlock(new block("photoCount", array(
            "tpc"       => 0,
            "totalUrl"  => "",
            "pc"        => $this->object->getPhotoCount(),
            "url"       => "photos.php?set_id=" . $this->object->getId() . "&_order=set_order"
        )));


        return $tpl;
    }

    private function getSelection() : ?selection {
        $selection=null;
        if (user::getCurrent()->canEditOrganizers()) {
            try {
                $selection=new selection($_SESSION, array(
                    "coverphoto"    => "set.php?_action=coverphoto&amp;set_id=" . $this->object->getId() . "&amp;coverphoto=",
                    "add to set"    => "set.php?_action=addphoto&amp;set_id=" . $this->object->getId() . "&amp;_photo_id=",
                    "return"        => "_return=set.php&amp;_qs=set_id=" . $this->object->getId()
                ));
            } catch (photoNoSelectionException $e) {
                $selection=null;
            }
        }

        return $selection;
    }

}
