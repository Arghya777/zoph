<?php
/**
 * View for edit set page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use conf\conf;
use template\block;
use template\form;
use web\request;

use set\model as set;
use user;

/**
 * This view displays the set page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        if ($this->object->getId() != 0) {
            $returnURL = "set.php?set_id=" . $this->object->getId();
        } else {
            $returnURL = "set.php?_action=sets";
        }

        if ($this->request["_action"] == "new") {
            return array(
                translate("return")    => $returnURL
            );
        } else {
            return array(
                translate("return")    => $returnURL,
                translate("new")       => "set.php?_action=new&amp;parent_set_id=" . $this->object->getId(),
                translate("delete")    => "set.php?_action=delete&amp;set_id=" . $this->object->getId()
            );
        }
    }

    /**
     * Output the view
     */
    public function view() : block {
        if ($this->request["_action"] == "new") {
            $action = "insert";
        } else if (in_array($this->request["_action"], array("edit", "insert"))) {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = $this->request["_action"];
        }
        $tpl = new block("main", array(
            "title"             => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());

        $form = new form("form", array(
            "formAction"    => "set.php",
            "class"         => "set",
            "onsubmit"      => null,
            "action"        => $action,
            "submit"        => translate($action, 0)
        ));


        $form->addInputHidden("set_id", $this->object->getId());
        $form->addInputText("name", $this->object->get("name"), translate("name"));

        $tpl->addBlock($form);

        return $tpl;

    }

}
