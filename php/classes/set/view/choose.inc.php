<?php
/**
 * View to choose a set
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use conf\conf;
use photo\collection as photoCollection;
use set\model as set;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Display screen for pagesets
 */
class choose extends view implements \view {

    /** @var holds photos to be added */
    private $photos;

    public function getActionLinks() : array {
        return array();
    }

    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"         => $this->getTitle(),
        ));

        if (empty($this->photos)) {
            $tpl->addBlock(new block("message", array(
                "class"     => "error",
                "text"      => translate("No photos were found matching your search criteria.")
            )));
        } else {
            $tpl->addBlock(new block("message", array(
                "class"     => "info",
                "text"      => sprintf(translate("Please choose the set you wish to add %d photos to"), count($this->photos))
            )));

            $form  = new form("form", array(
                "class"     => "set",
                "formAction" => "set.php",
                "action"    => "addphotos",
                "submit"    => translate("add to set")
            ));

            $form->addHiddenFields($this->request->getRequestVars(), array("_off", "_action"));

            $form->addDropdown("_set_id", set::getDropdown(), translate("Set"));

            $tpl->addBlock($form);

        }
        return $tpl;
    }

    public function getTitle() : string {
        return translate("Add photos to set", false);
    }

    public function setPhotos(photoCollection $photos) {
        $this->photos = $photos;
    }
}
