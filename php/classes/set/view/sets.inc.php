<?php
/**
 * View to display sets
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use conf\conf;
use set\model as set;
use template\block;
use web\request;

/**
 * Display screen for sets
 */
class sets extends view implements \view {
    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        return array(
            translate("new")       => "set.php?_action=new",
        );
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"         => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());
        $tpl->addBlock(set::getTable());
        return $tpl;
    }

    public function getTitle() : string {
        return translate("Sets", false);
    }
}
