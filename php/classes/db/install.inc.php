<?php
/**
 * Create Database & set access rights
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

use template\result;

/**
 * Database creation
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class install {

    /** @var array store database login details */
    private $db;

    private $results = array();

    private $host;

    public function __construct(string $root, string $rootpw) {
        $this->db = db::getLoginDetails();

        db::setLoginDetails($this->db["host"], "mysql", $root, $rootpw, $this->db["prefix"]);

        $this->host = $this->getHost();
    }

    public function create() :void {
        try {
            db::SQL("CREATE DATABASE IF NOT EXISTS " . $this->db["dbname"] .
                " CHARACTER SET utf8 COLLATE utf8_general_ci;");
            $this->results["create"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["create"] = result::ERROR;
            $this->errors["create"] = $e->getMessage();
        }
    }

    public function user() : void {
        try {
            db::SQL("CREATE USER IF NOT EXISTS " . $this->db["user"] . "@" . $this->host .
                " IDENTIFIED BY \"" . $this->db["pass"] . "\";");
            $this->results["user"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["user"] = result::ERROR;
            $this->errors["user"] = $e->getMessage();
        }
    }

    public function grant() : void {
        try {
            $rows=db::SQL("SELECT USER();")->fetch();

            $host=explode("@", ($rows[0]))[1];

            db::SQL("GRANT ALL ON " . $this->db["dbname"] . ".* " .
                "TO " . $this->db["user"] . "@" . $host . ";");
            $this->results["grant"] = result::SUCCESS;
        } catch (exception $e) {
            $this->results["grant"] = result::ERROR;
            $this->errors["grant"] = $e->getMessage();
        }
    }

    public function getHost() {
        try {
            $rows=db::SQL("SELECT USER();")->fetch();
            $this->results["host"] = result::SUCCESS;
            return explode("@", ($rows[0]))[1];
        } catch (exception $e) {
            $this->results["get host"] = result::ERROR;
            $this->errors["get host"] = $e->getMessage();
        }
    }

    public function getResults() : array {
        $results = array();
        foreach ($this->results as $key => $result) {
            $results[] = new result($result, $key, $this->errors[$key] ?? "");
        }
        return $results;
    }

}
