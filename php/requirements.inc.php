<?php
/**
 * This file checks if all the PHP requirements are available.
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

use conf\conf;
use requirements\check;
use requirements\view\failed;
use web\request;
if (basename($_SERVER["SCRIPT_NAME"]) == "css.php") {
    return;
}

if (!ini_get("date.timezone")) {
    @$tz=date("e");
    log::msg("You should set your timezone in php.ini, guessing it should be $tz",
        log::WARN, log::GENERAL);
    date_default_timezone_set($tz);
}

ini_set("session.use_only_cookies", true);

$check = new check();
$check->doChecks();

$status = $check->getStatus();
if (!$status) {
    $view = new failed(request::create());
    $view->addRequirements($check);
    $view->display();

    exit;
}
