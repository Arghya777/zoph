<?php
/**
 * This is a trait to add some common method to views for organisers
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

use geo\map;
use template\block;

/**
 * Common view methods for organisers
 *
 * @author Jeroen Roos
 * @package Zoph
 */
trait organiserView {
    private function getChildren() : ?block {
        $user = user::getCurrent();
        $order = $user->prefs->get("child_sortorder");
        $children = $this->object->getChildren($order);
        $_view=$this->request["_view"] ?? $user->prefs->get("view");
        $_autothumb=$this->request["_autothumb"] ?? $user->prefs->get("autothumb");

        if ($children) {
            return new block("view_" . $_view, array(
                "id" => $_view . "view",
                "items" => $children,
                "autothumb" => $_autothumb,
                "topnode" => true,
                "links" => array(
                    translate("view photos") => self::PHOTOLINK
                )
            ));
        } else {
            return null;
        }

    }

        /**
     * Get view
     * @return template\block view
     */
    public function view() : block {
        $user = user::getCurrent();
        $_view=$this->request["_view"] ?? $user->prefs->get("view");
        $_autothumb=$this->request["_autothumb"] ?? $user->prefs->get("autothumb");

        try {
            $pagenum = $this->request["_pageset_page"];
            $page=$this->object->getPage($this->request->getVars, $pagenum);
            $showOrig=$this->object->showOrig($pagenum);
        } catch (pageException $e) {
            $showOrig=true;
            $page=null;
        }

        $main=new block("organizer", array(
            "page"          => $page,
            "pageTop"       => $this->object->showPageOnTop(),
            "pageBottom"    => $this->object->showPageOnBottom(),
            "showMain"      => $showOrig,
            "title"         => $this->getTitle(),
            "ancLinks"      => $this->object->getAncestorLinks(),
            "selection"     => $this->getSelection(),
            "coverphoto"    => $this->object->displayCoverPhoto(),
            "description"   => $this->object->get(self::DESCRIPTION),
            "view"          => $_view,
            "view_name"     => self::VIEWNAME,
            "view_hidden"   => null,
            "autothumb"     => $_autothumb,
            "map"           => $this->getMap()
        ));

        $main->addActionlinks($this->getActionlinks());

        $main->addBlock($this->getPhotoCount());
        $children = $this->getChildren();
        if ($children instanceof block) {
            $main->addBlock($children);
        }
        return $main;
    }

    private function getPhotoCount() : block {
        $sortorder = $this->object->get("sortorder");
        $sort = $sortorder ? "&_order=" . $sortorder : "";

        return new block("photoCount", array(
            "tpc"       => $this->object->getTotalPhotoCount(),
            "totalUrl"  => self::PHOTOLINK . $this->object->getBranchIds() . $sort,
            "pc"        => $this->object->getPhotoCount(),
            "url"       => self::PHOTOLINK . $this->object->getId() . $sort
        ));
    }

    protected function getMap() : ?map {
        return null;
    }

}
